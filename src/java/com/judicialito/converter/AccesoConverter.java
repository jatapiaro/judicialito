/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.morado.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author jacobotapia
 */

@FacesConverter("accesoConverter")
public class AccesoConverter implements Converter{

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        return value;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        String sexo="";
        if(value!=null){
            switch ((int)value){
                case 0:
                    sexo="Policia";
                    break;
                case 1:
                    sexo="Personal del Ministerio Público";
                    break; 
                case 2:
                    sexo="Juez";
                    break;
                case 3:
                    sexo="Administrador";
                    break;           
            }
        } 
        return sexo;
    }
    
}
