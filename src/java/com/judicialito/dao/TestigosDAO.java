/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.judicialito.dao;

import com.judicialito.model.Testigo;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author jacobotapia
 */
public class TestigosDAO extends DAO{
    
    
    public void registrar(Testigo t) throws Exception{
        try{
            this.Conect();
            PreparedStatement ps=this.getConnection().prepareStatement(""
                    + "INSERT INTO Testigos (reporteID,rfc,nombre,apellidoPaterno,"
                    + "apellidoMaterno,testimonio,firma,direccionID,numeroExterior,"
                    + "numeroInterior,calle) VALUES(?,?,?,?,?,?,?,?,?,?,?)");
            ps.setInt(1, t.getReporteID());
            ps.setString(2, Encrypt(t.getRfc().toUpperCase()));
            ps.setString(3, Encrypt(t.getNombre()));
            ps.setString(4, Encrypt(t.getApellidoPaterno()));
            ps.setString(5, Encrypt(t.getApellidoMaterno()));
            ps.setString(6, Encrypt(t.getTestimonio()));
            ps.setString(7, Encrypt(t.getFirma()));
            ps.setInt(8, t.getDireccion().getCodigoPostal());
            ps.setString(9, Encrypt(t.getDireccion().getNumeroExterior()));
            ps.setString(10, Encrypt(t.getDireccion().getNumeroInterior()));
            ps.setString(11, Encrypt(t.getDireccion().getCalle()));
            ps.execute();
        }catch(Exception e){
            throw e;
        }finally{
            this.Close();
        }
    }
    
    public ArrayList<Testigo> listar(int idReporte) throws Exception{
        ResultSet rs;
        ArrayList<Testigo> testigos=new ArrayList<Testigo>();
        DireccionDAO dao;
        try{
            this.Conect();
            PreparedStatement ps=this.getConnection().prepareStatement(""
                    + "SELECT id,rfc,nombre,apellidoPaterno,"
                    + "apellidoMaterno,testimonio,firma,direccionID,numeroExterior,"
                    + "numeroInterior,calle FROM Testigos WHERE reporteID=? "
                    + "ORDER BY id DESC");
            ps.setInt(1, idReporte);
            rs=ps.executeQuery();
            while(rs.next()){
                Testigo t=new Testigo();
                t.setId(rs.getInt("id"));
                t.setRfc(Decrypt(rs.getString("rfc")));
                t.setNombre(Decrypt(rs.getString("nombre")));
                t.setApellidoPaterno(Decrypt(rs.getString("apellidoPaterno")));
                t.setApellidoMaterno(Decrypt(rs.getString("apellidoMaterno")));
                t.setTestimonio(Decrypt(rs.getString("testimonio")));
                t.setFirma(Decrypt(rs.getString("firma")));
                dao=new DireccionDAO();
                t.setDireccion(dao.direccion(rs.getInt("direccionID")));
                t.getDireccion().setCalle(Decrypt(rs.getString("calle")));
                t.getDireccion().setNumeroExterior(Decrypt(rs.getString("numeroExterior")));
                t.getDireccion().setNumeroInterior(Decrypt(rs.getString("numeroInterior")));
                testigos.add(t);
            }
        }catch(Exception e){
            throw e;
        }finally{
            this.Close();
            return testigos;
        }
    }
    
}
