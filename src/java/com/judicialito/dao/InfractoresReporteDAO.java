/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.judicialito.dao;

import com.judicialito.model.InfractorReporte;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author jacobotapia
 */
public class InfractoresReporteDAO extends DAO{
    
    public void registrar(InfractorReporte i) throws Exception{
        try{
            this.Conect();
            PreparedStatement ps=this.getConnection().prepareStatement(""
                    + "INSERT INTO InfractoresReporte (infractorID,reporteID,"
                    + "direccionID,delitos) VALUES(?,?,?,?)");
            ps.setInt(1, i.getInfractor().getId());
            ps.setInt(2, i.getReporteID());
            ps.setInt(3, i.getDireccion().getCodigoPostal());
            ps.setString(4, Encrypt(i.delitosString()));
            ps.execute();
        }catch(Exception e){
            throw e;
        }finally{
            this.Close();
        }
    }
    
    public ArrayList<InfractorReporte> listar(int reporteID) throws Exception{
        ResultSet rs;
        ArrayList<InfractorReporte> lista=new ArrayList<InfractorReporte>();
        InfractorDAO iDao;
        DireccionDAO dDao;
        try{
            this.Conect();
            PreparedStatement ps=this.getConnection().prepareStatement(""
                    + "SELECT id,infractorID,direccionID,delitos,hasImage "
                    + "FROM InfractoresReporte WHERE reporteID=? ORDER BY id DESC");
            ps.setInt(1, reporteID);
            rs=ps.executeQuery();
            while(rs.next()){
                InfractorReporte i=new InfractorReporte();
                i.setId(rs.getInt("id"));
                iDao=new InfractorDAO();
                i.setInfractor(iDao.getOneByID(rs.getInt("infractorID")));
                dDao=new DireccionDAO();
                i.setDireccion(dDao.direccion(rs.getInt("direccionID")));
                i.setReporteID(reporteID);
                String s=Decrypt(rs.getString("delitos"));
                String[] data=s.split(",");
                ArrayList<String> d=new ArrayList<String>();
                for(String a:data){
                    d.add(a);
                }
                i.setHasImage(rs.getBoolean("hasImage"));
                i.setDelitos(d);
                lista.add(i);
            }
        }catch(Exception e){
            throw e;
        }finally{
            this.Close();
            return lista;
        }
    }
    
    public void insertImage(UploadedFile f,int id) throws Exception{
        try {
            this.Conect();
            PreparedStatement ps = this.getConnection().prepareStatement(""
                    + "UPDATE InfractoresReporte SET imagen=?,hasImage=1 WHERE "
                    + "id=?");
            InputStream is = f.getInputstream();
            ps.setBinaryStream(1, f.getInputstream());
            ps.setInt(2, id);
            ps.executeUpdate();
        } catch (Exception e) {
            throw e;
        } finally {
            this.Close();
        }        
    }
    
    public byte[] getImage(int id) throws Exception {
        ResultSet rs;
        byte[] bytes = null;
        try {
            this.Conect();
            PreparedStatement ps = this.getConnection().prepareStatement(""
                    + "SELECT imagen FROM InfractoresReporte WHERE "
                    + "id=? AND hasImage=1");
            ps.setInt(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                Blob blob = rs.getBlob("imagen");
                int blobLength = (int) blob.length();
                bytes = blob.getBytes(1, blobLength);
                blob.free();
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Close();
            return bytes;
        }
    }
    
}
