/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.judicialito.dao;

import com.judicialito.model.Foto;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author jacobotapia
 */
public class FotosDAO extends DAO{
    
    public void registrar(UploadedFile f, Foto foto) throws Exception{
        
        try{
            this.Conect();
            PreparedStatement ps=this.getConnection().prepareStatement(""
                    + "INSERT INTO Fotos (reporteID,nombre,descripcion,foto) "
                    + "VALUES(?,?,?,?)");
            ps.setInt(1, foto.getReporteID());
            ps.setString(2, Encrypt(foto.getNombre()));
            ps.setString(3, Encrypt(foto.getDescripcion()));
            InputStream is=f.getInputstream();
            ps.setBinaryStream(4, f.getInputstream());
            ps.execute();
        }catch(Exception e){
            throw e;
        }finally{
            this.Close();
        }    
    }
    
    public ArrayList<Foto> fotos(int reporteID) throws Exception{
        ArrayList<Foto> fotos=new ArrayList<Foto>();
        ResultSet rs;
        try{
            this.Conect();
            PreparedStatement ps=this.getConnection().prepareCall(""
                    + "SELECT id,nombre,descripcion FROM Fotos WHERE reporteID=? ORDER BY id DESC");
            ps.setInt(1, reporteID);
            rs=ps.executeQuery();
            while(rs.next()){
                Foto f=new Foto();
                f.setId(rs.getInt("id"));
                f.setNombre(Decrypt(rs.getString("nombre")));
                f.setDescripcion(Decrypt(rs.getString("descripcion")));
                f.setReporteID(reporteID);
                fotos.add(f);
            }
        }catch(Exception e){
            throw e;
        }finally{
            this.Close();
            return fotos;
        }
    }
    
    public byte[] getFoto(int id) throws Exception{
        ResultSet rs;
        byte[] bytes = null;
        try {
            this.Conect();
            PreparedStatement ps = this.getConnection().prepareStatement(""
                    + "SELECT foto from Fotos WHERE id=?");
            ps.setInt(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                Blob blob = rs.getBlob("foto");
                int blobLength = (int) blob.length();
                bytes = blob.getBytes(1, blobLength);
                blob.free();
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Close();
            return bytes;
        }        
    }
    
}
