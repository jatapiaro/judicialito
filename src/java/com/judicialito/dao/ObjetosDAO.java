/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.judicialito.dao;

import com.judicialito.model.Objeto;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author jacobotapia
 */
public class ObjetosDAO extends DAO{
    
    public void registrar(Objeto o) throws Exception{
        try{
            this.Conect();
            PreparedStatement ps=this.getConnection().prepareStatement(""
                    + "INSERT INTO Objetos (reporteID,nombre,descripcion) "
                    + "VALUES(?,?,?)");
            ps.setInt(1, o.getIdReporte());
            ps.setString(2, Encrypt(o.getNombre()));
            ps.setString(3, Encrypt(o.getDescripcion()));
            ps.execute();
        }catch(Exception e){
            throw e;
        }finally{
            this.Close();
        }
    }
    
    public ArrayList<Objeto> listar(int reporteID) throws Exception{
        ResultSet rs;
        ArrayList<Objeto> lista=new ArrayList<Objeto>();
        try{
            this.Conect();
            PreparedStatement ps=this.getConnection().prepareStatement(""
                    + "SELECT id,nombre,descripcion,hasImage FROM Objetos WHERE "
                    + "reporteID=? ORDER BY id DESC");
            ps.setInt(1, reporteID);
            rs=ps.executeQuery();
            while(rs.next()){
                Objeto o=new Objeto();
                o.setId(rs.getInt("id"));
                o.setIdReporte(reporteID);
                o.setNombre(Decrypt(rs.getString("nombre")));
                o.setDescripcion(Decrypt(rs.getString("descripcion")));
                o.setHasImage(rs.getBoolean("hasImage"));
                lista.add(o);
            }
        }catch(Exception e){
            throw e;
        }finally{
            this.Close();
            return lista;
        }
        
    }
    
    
    public byte[] findOne(int id) throws Exception {
        ResultSet rs;
        byte[] bytes=null;
        try {
            this.Conect();
            PreparedStatement ps = this.getConnection().prepareStatement(""
                    + "SELECT imagen FROM Objetos WHERE "
                    + "id=? AND hasImage=1");
            ps.setInt(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                Blob blob = rs.getBlob("imagen");
                int blobLength = (int) blob.length();
                bytes=blob.getBytes(1, blobLength);
                blob.free();
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Close();
            return bytes;
        }
    }

    public void regFile(UploadedFile f, int id) throws Exception{
        try{
            this.Conect();
            PreparedStatement ps=this.getConnection().prepareStatement(""
                    + "UPDATE Objetos SET imagen=?,hasImage=1 WHERE "
                    + "id=?");
                InputStream is=f.getInputstream();
                ps.setBinaryStream(1, f.getInputstream());
                ps.setInt(2, id);
                ps.executeUpdate();
        }catch(Exception e){
            throw e;
        }finally{
            this.Close();
        }
    }
    
}
