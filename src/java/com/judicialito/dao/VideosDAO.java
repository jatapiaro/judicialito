/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.judicialito.dao;


import com.judicialito.model.Foto;
import com.judicialito.model.Video;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author jacobotapia
 */
public class VideosDAO extends DAO{

    public void registrar(UploadedFile f, Video video) throws Exception {

        try {
            this.Conect();
            PreparedStatement ps = this.getConnection().prepareStatement(""
                    + "INSERT INTO Videos (reporteID,nombre,descripcion,video) "
                    + "VALUES(?,?,?,?)");
            ps.setInt(1, video.getReporteID());
            ps.setString(2, Encrypt(video.getNombre()));
            ps.setString(3, Encrypt(video.getDescripcion()));
            InputStream is = f.getInputstream();
            ps.setBinaryStream(4, f.getInputstream());
            ps.execute();
        } catch (Exception e) {
            throw e;
        } finally {
            this.Close();
        }
    }

    public ArrayList<Video> videos(int reporteID) throws Exception {
        ArrayList<Video> videos = new ArrayList<Video>();
        ResultSet rs;
        CamaraDAO dao;
        try {
            this.Conect();
            PreparedStatement ps = this.getConnection().prepareCall(""
                    + "SELECT id,nombre,descripcion FROM Videos WHERE "
                    + "reporteID=? ORDER BY id DESC");
            ps.setInt(1, reporteID);
            rs = ps.executeQuery();
            while (rs.next()) {
                Video v = new Video();
                v.setId(rs.getInt("id"));
                v.setNombre(Decrypt(rs.getString("nombre")));
                v.setDescripcion(Decrypt(rs.getString("descripcion")));
                v.setReporteID(reporteID);
                videos.add(v);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Close();
            return videos;
        }
    }

    public byte[] getVideo(int id) throws Exception {
        ResultSet rs;
        byte[] bytes = null;
        try {
            this.Conect();
            PreparedStatement ps = this.getConnection().prepareStatement(""
                    + "SELECT video from Videos WHERE id=?");
            ps.setInt(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                Blob blob = rs.getBlob("video");
                int blobLength = (int) blob.length();
                bytes = blob.getBytes(1, blobLength);
                blob.free();
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Close();
            return bytes;
        }
    }

}
