/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.judicialito.dao;

import com.judicialito.model.Patrulla;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author jacobotapia
 */
public class PatrullaDAO extends DAO{
    
    public void registrar(Patrulla p) throws Exception{
        try{
            this.Conect();
            PreparedStatement ps=this.getConnection().prepareStatement(""
                    + "INSERT INTO Patrullas (placas,marca,modelo,sectorID) "
                    + "VALUES(?,?,?,?)");
            ps.setString(1, Encrypt(p.getPlacas()));
            ps.setString(2, Encrypt(p.getMarca()));
            ps.setString(3, Encrypt(p.getModelo()));
            ps.setInt(4, p.getSectorID());
            ps.execute();
        }catch(Exception e){
            throw e;
        }finally{
            this.Close();
        }
    }
    
    public ArrayList<Patrulla> patrullas() throws Exception{
        ArrayList<Patrulla> patrullas=new ArrayList<Patrulla>();
        ResultSet rs;
        try{
            this.Conect();
            PreparedStatement ps=this.getConnection().prepareStatement(""
                    + "SELECT id,placas,marca,modelo,sectorID from "
                    + "Patrullas WHERE activo=1");
            rs=ps.executeQuery();
            while(rs.next()){
                Patrulla p=new Patrulla();
                p.setId(rs.getInt("id"));
                p.setPlacas(Decrypt(rs.getString("placas")));
                p.setMarca(Decrypt(rs.getString("marca")));
                p.setModelo(Decrypt(rs.getString("modelo")));
                p.setSectorID(rs.getInt("sectorID"));
                patrullas.add(p);
            }
        }catch(Exception e){
            throw e;
        }finally{
            this.Close();
            return patrullas;
        }
    }
    
    public Patrulla getOneById(int id) throws Exception{
        Patrulla p=null;
        ResultSet rs;
        try{
            this.Conect();
            PreparedStatement ps=this.getConnection().prepareStatement(""
                    + "SELECT placas,marca,modelo,sectorID from "
                    + "Patrullas WHERE activo=1 AND id=?");
            ps.setInt(1, id);
            rs=ps.executeQuery();
            while(rs.next()){
                p=new Patrulla();
                p.setId(id);
                p.setPlacas(Decrypt(rs.getString("placas")));
                p.setMarca(Decrypt(rs.getString("marca")));
                p.setModelo(Decrypt(rs.getString("modelo")));
                p.setSectorID(rs.getInt("sectorID"));                
            }
        }catch(Exception e){
            throw e;
        }finally{
            this.Close();
            return p;
        }
    }
    
    public void update(Patrulla p) throws Exception{
        try{
            this.Conect();
            PreparedStatement ps=this.getConnection().prepareStatement(""
                    + "UPDATE Patrullas SET placas=?,marca=?,modelo=?,"
                    + "sectorID=? WHERE id=?");
            ps.setString(1, Encrypt(p.getPlacas()));
            ps.setString(2, Encrypt(p.getMarca()));
            ps.setString(3, Encrypt(p.getModelo()));
            ps.setInt(4, p.getSectorID());
            ps.setInt(5, p.getId());
            ps.executeUpdate();
        }catch(Exception e){
            throw e;
        }finally{
            this.Close();
        }
    }
    
    public void delete(int id) throws Exception{
        try{
            this.Conect();
            PreparedStatement ps=this.getConnection().prepareStatement(""
                    + "UPDATE Patrullas SET activo=0 WHERE id=?");
            ps.setInt(1, id);
            ps.executeUpdate();
        }catch(Exception e){
            throw e;
        }finally{
            this.Close();
        }
    }

    public Patrulla getOneByPlacas(String placas) throws Exception {
        Patrulla p = null;
        ResultSet rs;
        SectorDAO dao;
        try {
            this.Conect();
            PreparedStatement ps = this.getConnection().prepareStatement(""
                    + "SELECT id,marca,modelo,sectorID from "
                    + "Patrullas WHERE activo=1 AND placas=?");
            ps.setString(1, Encrypt(placas));
            rs = ps.executeQuery();
            while (rs.next()) {
                p = new Patrulla();
                p.setId(rs.getInt("id"));
                p.setPlacas(placas);
                p.setMarca(Decrypt(rs.getString("marca")));
                p.setModelo(Decrypt(rs.getString("modelo")));
                dao=new SectorDAO();
                p.setSector(dao.getOneByID(rs.getInt("sectorID")));
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Close();
            return p;
        }
    }
    
}
