/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.judicialito.dao;

import com.judicialito.model.Camara;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author jacobotapia
 */
public class CamaraDAO extends DAO{
    public void registrar(Camara c) throws Exception{
        try{
            this.Conect();
            PreparedStatement ps=this.getConnection().prepareStatement(""
                    + "INSERT INTO Camaras (identificador,idDireccion,"
                    + "calle) VALUES(?,?,?)");
            ps.setInt(1, c.getIdentificador());
            ps.setInt(2, c.getDireccion().getCodigoPostal());
            ps.setString(3, Encrypt(c.getDireccion().getCalle()));
            ps.execute();
        }catch(Exception e){
            throw e;
        }finally{
            this.Close();
        }
    }
    
    public ArrayList<Camara> camaras() throws Exception{
        ResultSet rs;
        ArrayList<Camara> camaras=new ArrayList<Camara>();
        try{
            this.Conect();
            PreparedStatement ps=this.getConnection().prepareStatement(""
                    + "SELECT id,identificador,idDireccion,calle FROM "
                    + "Camaras WHERE activo=1");
            rs=ps.executeQuery();
            while(rs.next()){
                Camara c=new Camara();
                c.setId(rs.getInt("id"));
                c.setIdentificador(rs.getInt("identificador"));
                DireccionDAO dao=new DireccionDAO();
                c.setDireccion(dao.direccion(rs.getInt("idDireccion")));
                c.getDireccion().setCalle(Decrypt(rs.getString("calle")));
                camaras.add(c);
            }
        }catch(Exception e){
            throw e;
        }finally{
            this.Close();
            return camaras;
        }
    }
    
    public Camara getOneByID(int id) throws Exception{
       ResultSet rs;
       Camara c=null;
       try{
           this.Conect();
           PreparedStatement ps=this.getConnection().prepareStatement(""
                   + "SELECT identificador,idDireccion,calle FROM Camaras "
                   + "WHERE id=? AND activo=1");
           ps.setInt(1, id);
           rs=ps.executeQuery();
           while(rs.next()){
               c=new Camara();
               c.setId(id);
                c.setIdentificador(rs.getInt("identificador"));
                DireccionDAO dao=new DireccionDAO();
                c.setDireccion(dao.direccion(rs.getInt("idDireccion")));
                c.getDireccion().setCalle(Decrypt(rs.getString("calle")));              
           }
       }catch(Exception e){
           throw e;
       }finally{
           this.Close();
           return c;
       }
    }
    
    public void update(Camara c) throws Exception{
        try{
            this.Conect();
            PreparedStatement ps=this.getConnection().prepareStatement(""
                    + "UPDATE Camaras SET identificador=?,idDireccion=?,calle=? "
                    + "WHERE id=? AND activo=1");
            ps.setInt(1, c.getIdentificador());
            ps.setInt(2, c.getDireccion().getCodigoPostal());
            ps.setString(3,Encrypt(c.getDireccion().getCalle()));
            ps.setInt(4, c.getId());
            ps.executeUpdate();
        }catch(Exception e){
            throw e;
        }finally{
            this.Close();
        }
    }
    
    public void delete(int id) throws Exception{
        try{
            this.Conect();
            PreparedStatement ps=this.getConnection().prepareStatement(""
                    + "UPDATE Camaras SET activo=0 WHERE id=?");
            ps.setInt(1, id);
            ps.executeUpdate();
        }catch(Exception e){
            throw e;
        }finally{
            this.Close();
        }
    }
    
}
