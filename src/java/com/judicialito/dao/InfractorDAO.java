/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.judicialito.dao;

import com.judicialito.model.Infractor;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author jacobotapia
 */
public class InfractorDAO extends DAO{
   
    public void registrar(Infractor i) throws Exception{
        try{
            this.Conect();
            PreparedStatement ps=this.getConnection().prepareStatement(""
                    + "INSERT INTO Infractores (rfc,nombre,apellidoPaterno,"
                    + "apellidoMaterno,genero,etnia,nivelEducacion,religion,"
                    + "fechaNacimiento) VALUES(?,?,?,?,?,?,?,?,?)");
            ps.setString(1, Encrypt(i.getRfc().toUpperCase()));
            ps.setString(2, Encrypt(i.getNombre()));
            ps.setString(3, Encrypt(i.getApellidoPaterno()));
            ps.setString(4, Encrypt(i.getApellidoMaterno()));
            ps.setString(5, i.getGenero());
            ps.setString(6, i.getEtnia());
            ps.setString(7, i.getNivelEducacion());
            ps.setString(8, i.getReligion());
            ps.setDate(9, this.GetSQLDate(i.getFechaNacimiento()));
            ps.execute();
        }catch(Exception e){
            throw e;
        }finally{
            this.Close();
        }
    }
    
    public ArrayList<Infractor> infractores() throws Exception{
        ArrayList<Infractor> infractores=new ArrayList<Infractor>();
        ResultSet rs;
        try{
            this.Conect();
            PreparedStatement ps=this.getConnection().prepareStatement(""
                    + "SELECT id,rfc,nombre,apellidoPaterno,apellidoMaterno,"
                    + "genero,etnia,nivelEducacion,religion,fechaNacimiento "
                    + "FROM Infractores");
            rs=ps.executeQuery();
            while(rs.next()){
                Infractor i=new Infractor();
                i.setId(rs.getInt("id"));
                i.setRfc(Decrypt(rs.getString("rfc")));
                i.setNombre(Decrypt(rs.getString("nombre")));
                i.setApellidoPaterno(Decrypt(rs.getString("apellidoPaterno")));
                i.setApellidoMaterno(Decrypt(rs.getString("apellidoMaterno")));
                i.setGenero(rs.getString("genero"));
                i.setEtnia(rs.getString("etnia"));
                i.setNivelEducacion(rs.getString("nivelEducacion"));
                i.setReligion(rs.getString("religion"));
                i.setFechaNacimiento(rs.getDate("fechaNacimiento"));
                infractores.add(i);
            }
        }catch(Exception e){
            throw e;
        }finally{
            this.Close();
            return infractores;
        }
    }
    
    public Infractor getOneByID(int id) throws Exception{
        Infractor i=null;
        ResultSet rs;
        try{
            this.Conect();
            PreparedStatement ps=this.getConnection().prepareStatement(""
                    + "SELECT rfc,nombre,apellidoPaterno,apellidoMaterno,"
                    + "genero,etnia,nivelEducacion,religion,fechaNacimiento "
                    + "FROM Infractores WHERE id=?");
            ps.setInt(1, id);
            rs=ps.executeQuery();
            while(rs.next()){
                i=new Infractor();
                i.setId(id);
                i.setRfc(Decrypt(rs.getString("rfc")));
                i.setNombre(Decrypt(rs.getString("nombre")));
                i.setApellidoPaterno(Decrypt(rs.getString("apellidoPaterno")));
                i.setApellidoMaterno(Decrypt(rs.getString("apellidoMaterno")));
                i.setGenero(rs.getString("genero"));
                i.setEtnia(rs.getString("etnia"));
                i.setNivelEducacion(rs.getString("nivelEducacion"));
                i.setReligion(rs.getString("religion"));
                i.setFechaNacimiento(rs.getDate("fechaNacimiento"));                
            }
        }catch(Exception e){
            throw e;
        }finally{
            this.Close();
            return i;
        }
    }
    
    public Infractor getOneByRFC(String rfc) throws Exception {
        Infractor i = null;
        ResultSet rs;
        try {
            this.Conect();
            PreparedStatement ps = this.getConnection().prepareStatement(""
                    + "SELECT id,nombre,apellidoPaterno,apellidoMaterno,"
                    + "genero,etnia,nivelEducacion,religion,fechaNacimiento "
                    + "FROM Infractores WHERE rfc=?");
            ps.setString(1, Encrypt(rfc));
            rs = ps.executeQuery();
            while (rs.next()) {
                i = new Infractor();
                i.setId(rs.getInt("id"));
                i.setRfc(rfc);
                i.setNombre(Decrypt(rs.getString("nombre")));
                i.setApellidoPaterno(Decrypt(rs.getString("apellidoPaterno")));
                i.setApellidoMaterno(Decrypt(rs.getString("apellidoMaterno")));
                i.setGenero(rs.getString("genero"));
                i.setEtnia(rs.getString("etnia"));
                i.setNivelEducacion(rs.getString("nivelEducacion"));
                i.setReligion(rs.getString("religion"));
                i.setFechaNacimiento(rs.getDate("fechaNacimiento"));
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Close();
            return i;
        }
    }    
    
    public void update(Infractor i) throws Exception{
        try{
            this.Conect();
            PreparedStatement ps=this.getConnection().prepareStatement(""
                    + "UPDATE Infractores SET nombre=?,apellidoPaterno=?,"
                    + "apellidoMaterno=?,genero=?,etnia=?,"
                    + "nivelEducacion=?,religion=?,fechaNacimiento=? "
                    + "WHERE id=?");
            ps.setString(1, Encrypt(i.getNombre()));
            ps.setString(2, Encrypt(i.getApellidoPaterno()));
            ps.setString(3, Encrypt(i.getApellidoMaterno()));
            ps.setString(4, i.getGenero());
            ps.setString(5, i.getEtnia());
            ps.setString(6, i.getNivelEducacion());
            ps.setString(7, i.getReligion());
            ps.setDate(8, this.GetSQLDate(i.getFechaNacimiento()));
            ps.setInt(9, i.getId());
            ps.executeUpdate();
        }catch(Exception e){
            throw e;
        }finally{
            this.Close();
        }
    }
    
}
