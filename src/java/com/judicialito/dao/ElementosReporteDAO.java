/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.judicialito.dao;

import com.judicialito.model.ElementosReporte;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author jacobotapia
 */
public class ElementosReporteDAO extends DAO{
    
    public void registrar(ElementosReporte e) throws Exception{
        try{
            this.Conect();
            PreparedStatement ps=this.getConnection().prepareStatement(""
                    + "INSERT INTO ElementosReporte (reporteID,elementoID,patrullaID) "
                    + "VALUES (?,?,?)");
            ps.setInt(1, e.getReporteID());
            ps.setInt(2, e.getElemento().getId());
            ps.setInt(3, e.getPatrulla().getId());
            ps.execute();
        }catch(Exception ex){
            throw ex;
        }finally{
            this.Close();
        }
    }
    
    public ArrayList<ElementosReporte> listar(int reporteID) throws Exception{
        ArrayList<ElementosReporte> lista=new ArrayList<ElementosReporte>();
        ResultSet rs;
        PatrullaDAO pDao;
        ElementoDAO eDao;
        try{
            this.Conect();
            PreparedStatement ps=this.getConnection().prepareStatement(""
                    + "SELECT id,elementoID,patrullaID,parteInformativo,"
                    + "contesto FROM ElementosReporte WHERE reporteID=? ORDER BY id DESC");
            ps.setInt(1, reporteID);
            rs=ps.executeQuery();
            while(rs.next()){
                ElementosReporte er=new ElementosReporte();
                er.setId(rs.getInt("id"));
                er.setReporteID(reporteID);
                pDao=new PatrullaDAO();
                er.setPatrulla(pDao.getOneById(rs.getInt("patrullaID")));
                eDao=new ElementoDAO();
                er.setElemento(eDao.getOneByID(rs.getInt("elementoID")));
                String s=rs.getString("parteInformativo");
                if(s!=null){
                    er.setParteInformativo(Decrypt(s));
                }else{
                    er.setParteInformativo("");
                }
                er.setContesto(rs.getBoolean("contesto"));
                lista.add(er);
            }
        }catch(Exception e){
            throw e;
        }finally{
            this.Close();
            return lista;
        }
    }
    
    public void registrarParteInformativo(ElementosReporte er) throws Exception{
        try{
            this.Conect();
            PreparedStatement ps=this.getConnection().prepareStatement(""
                    + "UPDATE ElementosReporte SET parteInformativo=?,"
                    + "contesto=1 WHERE id=?");
            ps.setString(1, Encrypt(er.getParteInformativo()));
            ps.setInt(2, er.getId());
            ps.executeUpdate();
        }catch(Exception e){
            throw e;
        }finally{
            this.Close();
        }
    }
    
    
    public ElementosReporte findOne(int id) throws Exception {
        /*
        *Only to set the update
        */
        ElementosReporte er = null;
        ResultSet rs;
        PatrullaDAO pDao;
        ElementoDAO eDao;
        try {
            this.Conect();
            PreparedStatement ps = this.getConnection().prepareStatement(""
                    + "SELECT reporteID,elementoID,patrullaID,parteInformativo,"
                    + "contesto FROM ElementosReporte WHERE id=?");
            ps.setInt(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                er = new ElementosReporte();
                er.setId(id);
                er.setReporteID(rs.getInt("reporteID"));
                pDao = new PatrullaDAO();
                er.setPatrulla(pDao.getOneById(rs.getInt("patrullaID")));
                eDao = new ElementoDAO();
                er.setElemento(eDao.getOneByID(rs.getInt("elementoID")));
                String s = rs.getString("parteInformativo");
                if (s != null) {
                    er.setParteInformativo(Decrypt(s));
                } else {
                    er.setParteInformativo("");
                }
                er.setContesto(rs.getBoolean("contesto"));
                
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Close();
            return er;
        }
    }    
    
}
