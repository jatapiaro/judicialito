/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.judicialito.dao;

import com.judicialito.model.Denunciante;
import com.judicialito.model.Direccion;
import com.judicialito.model.Infractor;
import com.judicialito.model.Reporte;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;

/**
 *
 * @author jacobotapia
 */
public class ReporteDAO extends DAO{
    
    public int registrar(Reporte reporte) throws Exception{
        ResultSet rs;
        int denuncianteID=0;
        int inserted=0;
        try{
            this.Conect();
            PreparedStatement ps=this.getConnection().prepareStatement(""
                    + "INSERT INTO Denunciantes (rfc,nombre,apellidoPaterno,"
                    + "apellidoMaterno,firma,direccionID,numeroExterior,"
                    + "numeroInterior,calle) VALUES(?,?,?,?,?,?,?,?,?)",Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, Encrypt(reporte.getDenunciante().getRfc().toUpperCase()));
            ps.setString(2, Encrypt(reporte.getDenunciante().getNombre()));
            ps.setString(3, Encrypt(reporte.getDenunciante().getApellidoPaterno()));
            ps.setString(4, Encrypt(reporte.getDenunciante().getApellidoMaterno()));
            ps.setString(5, Encrypt(reporte.getDenunciante().getFirma()));
            ps.setInt(6, reporte.getDenunciante().getDireccion().getCodigoPostal());
            ps.setString(7, Encrypt(reporte.getDenunciante().getDireccion().getNumeroExterior()));
            ps.setString(8, Encrypt(reporte.getDenunciante().getDireccion().getNumeroInterior()));
            ps.setString(9, Encrypt(reporte.getDenunciante().getDireccion().getCalle()));
            ps.execute();
            rs = ps.getGeneratedKeys();
            while(rs.next()){
                denuncianteID=rs.getInt(1);
            }
            ps.close();
            ResultSet rs2;
            PreparedStatement ps2=this.getConnection().prepareStatement(""
                    + "INSERT INTO Reportes (sectorID,titulo,hecho,"
                    + "folio,direccionID,denuncianteID,fechaHora,camaraID,calle) VALUES("
                    + "?,?,?,?,?,?,?,?,?)",Statement.RETURN_GENERATED_KEYS);
            ps2.setInt(1, reporte.getSectorID());
            ps2.setString(2, Encrypt(reporte.getTitulo()));
            ps2.setString(3, Encrypt(reporte.getHecho()));
            ps2.setString(4, Encrypt(reporte.getFolio()));
            ps2.setInt(5, reporte.getDireccion().getCodigoPostal());
            ps2.setInt(6, denuncianteID);
            ps2.setTimestamp(7, this.getTimestamp(reporte.getFechaHora()));
            ps2.setInt(8, reporte.getCamara().getId());
            ps2.setString(9,Encrypt(reporte.getDireccion().getCalle()));
            ps2.execute();
            rs2 = ps2.getGeneratedKeys();
            while(rs2.next()){
                inserted=rs2.getInt(1);
            }            
        }catch(Exception e){
            throw e;
        }finally{
            this.Close();
            return inserted;
        }
    }
    
    public Reporte getOneByID(int id) throws Exception{
        ResultSet rs;
        Reporte r=null;
        SectorDAO daoS;
        DenuncianteDAO daoD;
        DireccionDAO daoDir;
        CamaraDAO daoC;
        ElementosReporteDAO eDao;
        ObjetosDAO oDao;
        InfractoresReporteDAO iDao;
        TestigosDAO tDao;
        FotosDAO fDao;
        VideosDAO vDao;
        try{
            this.Conect();
            PreparedStatement ps=this.getConnection().prepareStatement(""
                    + "SELECT sectorID,titulo,hecho,folio,direccionID,denuncianteID,"
                    + "fechaHora,camaraID,calle FROM Reportes WHERE id=?");
            ps.setInt(1, id);
            rs=ps.executeQuery();
            while(rs.next()){
                r=new Reporte();
                r.setId(id);
                eDao=new ElementosReporteDAO();
                r.setElementosReporte(eDao.listar(id));
               
                oDao=new ObjetosDAO();
                r.setObjetos(oDao.listar(id));
                
                iDao=new InfractoresReporteDAO();
                r.setInfractores(iDao.listar(id));
                
                tDao=new TestigosDAO();
                r.setTestigos(tDao.listar(id));
                
                fDao=new FotosDAO();
                r.setFotos(fDao.fotos(id));
                
                vDao=new VideosDAO();
                r.setVideos(vDao.videos(id));
                
                daoS=new SectorDAO();
                r.setSector(daoS.getOneByID(rs.getInt("sectorID")));
                r.setTitulo(Decrypt(rs.getString("titulo")));
                r.setHecho(Decrypt(rs.getString("hecho")));
                r.setFolio(Decrypt(rs.getString("folio")));
                daoDir=new DireccionDAO();
                r.setDireccion(daoDir.direccion(rs.getInt("direccionID")));
                daoD=new DenuncianteDAO();
                r.setDenunciante(daoD.getOneByID(rs.getInt("denuncianteID")));
                r.setTimestamp((rs.getTimestamp("fechaHora")));   
                daoC=new CamaraDAO();
                r.setCamara(daoC.getOneByID(rs.getInt("camaraID")));
                r.getDireccion().setCalle(Decrypt(rs.getString("calle")));
            }
        }catch(Exception e){
            throw e;
        }finally{
            this.Close();
            return r;
        }
    }
    
    public ArrayList<Reporte> listar() throws Exception{
        ResultSet rs;
        ArrayList<Reporte> reportes=new ArrayList<Reporte>();
        SectorDAO daoS;
        DenuncianteDAO daoD;
        DireccionDAO daoDir;
        CamaraDAO daoC;
        try{
            this.Conect();
            PreparedStatement ps=this.getConnection().prepareStatement(""
                    + "SELECT id,sectorID,titulo,hecho,folio,direccionID,"
                    + "denuncianteID,fechaHora,camaraID,calle FROM Reportes");
            rs=ps.executeQuery();
            while(rs.next()){
                Reporte r = new Reporte();
                r.setId(rs.getInt("id"));
                daoS = new SectorDAO();
                r.setSector(daoS.getOneByID(rs.getInt("sectorID")));
                r.setTitulo(Decrypt(rs.getString("titulo")));
                r.setHecho(Decrypt(rs.getString("hecho")));
                r.setFolio(Decrypt(rs.getString("folio")));
                daoDir = new DireccionDAO();
                r.setDireccion(daoDir.direccion(rs.getInt("direccionID")));
                daoD = new DenuncianteDAO();
                r.setDenunciante(daoD.getOneByID(rs.getInt("denuncianteID")));
                r.setTimestamp((rs.getTimestamp("fechaHora")));
                daoC = new CamaraDAO();
                r.setCamara(daoC.getOneByID(rs.getInt("camaraID")));
                r.getDireccion().setCalle(Decrypt(rs.getString("calle")));
                reportes.add(r);
            }
        }catch(Exception e){
            throw e;
        }finally{
           this.Close();
            
        }
        return reportes;
    }
    
    
    public ArrayList<Reporte> listar(int infractorID) throws Exception {
        ResultSet rs;
        ArrayList<Reporte> reportes = new ArrayList<Reporte>();
        DireccionDAO daoDir;
        try {
            
            this.Conect();
            PreparedStatement ps = this.getConnection().prepareStatement(""
                    + "SELECT Reportes.id,titulo,hecho,folio,Reportes.direccionID,"
                    + "InfractoresReporte.delitos FROM Reportes INNER JOIN InfractoresReporte "
                    + "WHERE InfractoresReporte.infractorID=? ORDER BY id DESC");
            ps.setInt(1, infractorID);
            rs = ps.executeQuery();
            while (rs.next()) {
                Reporte r = new Reporte();
                r.setId(rs.getInt("id"));
                r.setTitulo(Decrypt(rs.getString("titulo")));
                r.setHecho(Decrypt(rs.getString("hecho")));
                r.setFolio(Decrypt(rs.getString("folio")));
                daoDir=new DireccionDAO();
                r.setDireccion(daoDir.direccion(rs.getInt("direccionID")));
                r.getDireccion().setCalle(Decrypt(rs.getString("delitos")));
                Infractor f=new Infractor();
                boolean add=true;
                for(int i=0;i<reportes.size();i++){
                    if(reportes.get(i).getFolio().equals(r.getFolio())){
                        add=false;
                        break;
                    }
                }
                if(add){
                    reportes.add(r);
                }
                
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Close();

        }
        return reportes;
    }
    
    public Timestamp getTimestamp(java.util.Date date){
      return date == null ? null : new java.sql.Timestamp(date.getTime());
    }
 
}
