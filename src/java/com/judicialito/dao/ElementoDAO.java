/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.judicialito.dao;

import com.judicialito.model.Elemento;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author jacobotapia
 */
public class ElementoDAO extends DAO{
    
    public void registrar(Elemento e) throws Exception{
        try{
            this.Conect();
            PreparedStatement ps=this.getConnection().prepareCall(""
                    + "INSERT INTO Elementos (nivelAcceso,sectorID,nombre,"
                    + "apellidoPaterno,apellidoMaterno,placa,password) "
                    + "VALUES(?,?,?,?,?,?,?)");
            ps.setInt(1, e.getNivelAcceso());
            ps.setInt(2, e.getSectorID());
            ps.setString(3, Encrypt(e.getNombre()));
            ps.setString(4, Encrypt(e.getApellidoPaterno()));
            ps.setString(5, Encrypt(e.getApellidoMaterno()));
            ps.setString(6, Encrypt(e.getPlaca()));
            ps.setString(7, Encrypt(e.getPassword()));
            ps.execute();
        }catch(Exception ex){
            throw ex;
        }finally{
           this.Close();
        }
    }
    
    public ArrayList<Elemento> elementos() throws Exception{
        ArrayList<Elemento> elementos=new ArrayList<Elemento>();
        ResultSet rs;
        try{
            this.Conect();
            PreparedStatement ps=this.getConnection().prepareStatement(""
                    + "SELECT id,nivelAcceso,sectorID,nombre,apellidoPaterno,"
                    + "apellidoMaterno,placa FROM Elementos "
                    + "WHERE activo=1");
            rs=ps.executeQuery();
            while(rs.next()){
                Elemento e=new Elemento();
                e.setId(rs.getInt("id"));
                e.setNivelAcceso(rs.getInt("nivelAcceso"));
                e.setSectorID(rs.getInt("sectorID"));
                e.setNombre(Decrypt(rs.getString("nombre")));
                e.setApellidoPaterno(Decrypt(rs.getString("apellidoPaterno")));
                e.setApellidoMaterno(Decrypt(rs.getString("apellidoMaterno")));
                e.setPlaca(Decrypt(rs.getString("placa")));
                elementos.add(e);
            }
        }catch(Exception ex){
            throw ex;
        }finally{
            this.Close();
            return elementos;
        }
    }
    
    public Elemento getOneByID(int id) throws Exception{
        Elemento e=null;
        ResultSet rs;
        SectorDAO dao;
        try{
            this.Conect();
            PreparedStatement ps=this.getConnection().prepareStatement(""
                    + "SELECT nivelAcceso,sectorID,nombre,apellidoPaterno,"
                    + "apellidoMaterno,placa,password FROM Elementos "
                    + "WHERE activo=1 AND id=?");
            ps.setInt(1, id);
            rs=ps.executeQuery();
            while(rs.next()){
                e=new Elemento();
                e.setId(id);
                e.setNivelAcceso(rs.getInt("nivelAcceso"));
                dao=new SectorDAO();
                e.setSector(dao.getOneByID(rs.getInt("sectorID")));
                e.setNombre(Decrypt(rs.getString("nombre")));
                e.setApellidoPaterno(Decrypt(rs.getString("apellidoPaterno")));
                e.setApellidoMaterno(Decrypt(rs.getString("apellidoMaterno")));
                e.setPlaca(Decrypt(rs.getString("placa")));                
            }
        }catch(Exception ex){
            throw ex;
        }finally{
            this.Close();
            return e;
        }
    }
    
    public void update(Elemento e) throws Exception{
        try{
            this.Conect();
            PreparedStatement ps=this.getConnection().prepareStatement(""
                    + "UPDATE Elementos SET nivelAcceso=?,sectorID=?,"
                    + "nombre=?,apellidoPaterno=?,apellidoMaterno=? "
                    + "WHERE activo=1 AND id=?");
            ps.setInt(1, e.getNivelAcceso());
            ps.setInt(2, e.getSectorID());
            ps.setString(3, Encrypt(e.getNombre()));
            ps.setString(4, Encrypt(e.getApellidoPaterno()));
            ps.setString(5, Encrypt(e.getApellidoMaterno()));
            ps.setInt(6, e.getId());
            ps.executeUpdate();
        }catch(Exception ex){
            throw ex;
        }finally{
            this.Close();
        }
    }
    
    public void delete(int id) throws Exception{
        try{
            this.Conect();
            PreparedStatement ps=this.getConnection().prepareStatement(""
                    + "UPDATE Elementos SET activo=0 "
                    + "WHERE id=?");
            ps.setInt(1, id);
            ps.executeUpdate();
        }catch(Exception ex){
            throw ex;
        }finally{
            this.Close();
        }
    }

    public Elemento login(String placa,String psw) throws Exception {
        Elemento e = null;
        ResultSet rs;
        try {
            this.Conect();
            PreparedStatement ps = this.getConnection().prepareStatement(""
                    + "SELECT id,nivelAcceso,sectorID,nombre,apellidoPaterno,"
                    + "apellidoMaterno,placa FROM Elementos "
                    + "WHERE activo=1 AND placa=? AND password=?");
            ps.setString(1, Encrypt(placa));
            ps.setString(2, Encrypt(psw));
            rs = ps.executeQuery();
            while (rs.next()) {
                e = new Elemento();
                e.setId(rs.getInt("id"));
                e.setNivelAcceso(rs.getInt("nivelAcceso"));
                e.setSectorID(rs.getInt("sectorID"));
                e.setNombre(Decrypt(rs.getString("nombre")));
                e.setApellidoPaterno(Decrypt(rs.getString("apellidoPaterno")));
                e.setApellidoMaterno(Decrypt(rs.getString("apellidoMaterno")));
                e.setPlaca(Decrypt(rs.getString("placa")));
            }
        } catch (Exception ex) {
            throw ex;
        } finally {
            this.Close();
            return e;
        }

    }
    
    
    public Elemento getOneByPlaca(String placa) throws Exception {
        Elemento e = null;
        ResultSet rs;
        SectorDAO dao;
        try {
            this.Conect();
            PreparedStatement ps = this.getConnection().prepareStatement(""
                    + "SELECT id,nivelAcceso,sectorID,nombre,apellidoPaterno,"
                    + "apellidoMaterno,password FROM Elementos "
                    + "WHERE activo=1 AND placa=? AND nivelAcceso=0");
            ps.setString(1, Encrypt(placa));
            rs = ps.executeQuery();
            while (rs.next()) {
                e = new Elemento();
                e.setId(rs.getInt("id"));
                e.setPlaca(placa);
                e.setNivelAcceso(rs.getInt("nivelAcceso"));
                dao=new SectorDAO();
                e.setSector(dao.getOneByID(rs.getInt("sectorID")));
                e.setNombre(Decrypt(rs.getString("nombre")));
                e.setApellidoPaterno(Decrypt(rs.getString("apellidoPaterno")));
                e.setApellidoMaterno(Decrypt(rs.getString("apellidoMaterno")));
            }
        } catch (Exception ex) {
            throw ex;
        } finally {
            this.Close();
            return e;
        }
    }       
    
    
}
