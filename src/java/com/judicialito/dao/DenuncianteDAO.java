/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.judicialito.dao;

import com.judicialito.model.Denunciante;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author jacobotapia
 */
public class DenuncianteDAO extends DAO{
    
    public Denunciante getOneByID(int id) throws Exception{
        Denunciante d=null;
        ResultSet rs;
        DireccionDAO dao;
        try{
            this.Conect();
            PreparedStatement ps=this.getConnection().prepareStatement(""
                    + "SELECT rfc,nombre,apellidoPaterno,apellidoMaterno,"
                    + "firma,direccionID,numeroExterior,numeroInterior,"
                    + "calle FROM Denunciantes WHERE id=?");
            ps.setInt(1, id);
            rs=ps.executeQuery();
            while(rs.next()){
                d=new Denunciante();
                d.setId(id);
                d.setRfc(Decrypt(rs.getString("rfc")));
                d.setNombre(Decrypt(rs.getString("nombre")));
                d.setApellidoPaterno(Decrypt(rs.getString("apellidoPaterno")));
                d.setApellidoMaterno(Decrypt(rs.getString("apellidoMaterno")));
                d.setFirma(Decrypt(rs.getString("firma")));
                dao=new DireccionDAO();
                d.setDireccion(dao.direccion(rs.getInt("direccionID")));
                d.getDireccion().setNumeroExterior(Decrypt(rs.getString("numeroExterior")));
                d.getDireccion().setNumeroInterior(Decrypt(rs.getString("numeroInterior")));
                d.getDireccion().setCalle(Decrypt(rs.getString("calle")));
            }
        }catch(Exception e){
            throw e;
        }finally{
            this.Close();
            return d;
        }
        
    }
    
}
