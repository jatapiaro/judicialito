/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.judicialito.dao;

import com.judicialito.model.Direccion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;


/**
 *
 * @author jacobotapia
 */
public class DireccionDAO extends DAO{
    
    public Direccion direccion(int cp) throws Exception{
        ResultSet rs;
        Direccion d=null;
        try{
            this.Conect();
            PreparedStatement ps=this.getConnection().prepareStatement(""
                    + "SELECT colonia,estado,municipio FROM CodigosPostales "
                    + "WHERE codigoPostal=?");
            ps.setInt(1, cp);
            rs=ps.executeQuery();
            while(rs.next()){
                d=new Direccion();
                d.setCodigoPostal(cp);
                d.setColonia(Decrypt(rs.getString("colonia")));
                d.setEstado(Decrypt(rs.getString("estado")));
                d.setMunicipio(Decrypt(rs.getString("municipio")));
            }
        }catch(Exception e){
            throw e;
        }finally{
            this.Close();  
            
        }
        return d;
        
    }
}
