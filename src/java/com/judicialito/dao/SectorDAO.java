/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.judicialito.dao;

import com.judicialito.model.Sector;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author jacobotapia
 */
public class SectorDAO extends DAO{
    
    public void registrar(Sector s) throws Exception{
        try{
            this.Conect();
            PreparedStatement ps=this.getConnection().prepareStatement(""
                    + "INSERT INTO Sector (identificador,nombre) VALUES (?,?)");
            ps.setInt(1, s.getIdentificador());
            ps.setString(2, Encrypt(s.getNombre()));
            ps.execute();            
        }catch(Exception e){
            throw e;
        }finally{
            this.Close();
        }
    }
    
    public ArrayList<Sector> sectores() throws Exception{
        ArrayList<Sector> sectores=new ArrayList<Sector>();
        ResultSet rs;
        try{
            this.Conect();
            PreparedStatement ps=this.getConnection().prepareStatement(""
                    + "SELECT id,identificador,nombre FROM Sector WHERE activo=1");
            rs=ps.executeQuery();
            while(rs.next()){
                Sector s=new Sector();
                s.setId(rs.getInt("id"));
                s.setIdentificador(rs.getInt("identificador"));
                s.setNombre(Decrypt(rs.getString("nombre")));
                sectores.add(s);
            }
        }catch(Exception e){
            throw e;
        }finally{
            this.Close();
            return sectores;
        }
    }
    
    
    public Sector getOneByID(int id) throws Exception{
        Sector s=null;
        ResultSet rs;
        try{
            this.Conect();
            PreparedStatement ps=this.getConnection().prepareStatement(""
                    + "SELECT identificador,nombre FROM Sector WHERE activo=1 "
                    + "AND id=?");
            ps.setInt(1, id);
            rs=ps.executeQuery();
            while(rs.next()){
                s=new Sector();
                s.setId(id);
                s.setIdentificador(rs.getInt("identificador"));
                s.setNombre(Decrypt(rs.getString("nombre")));
            }
        }catch(Exception e){
            throw e;
        }finally{
            this.Close();
            return s;
        }
    }    
    
    public void update(Sector s) throws Exception{
        try{
            this.Conect();
            PreparedStatement ps=this.getConnection().prepareStatement(""
                    + "UPDATE Sector SET identificador=?,nombre=? WHERE "
                    + "id=?");
            ps.setInt(1, s.getIdentificador());
            ps.setString(2, Encrypt(s.getNombre()));
            ps.setInt(3, s.getId());
            ps.executeUpdate();
        }catch(Exception e){
            throw e;
        }finally{
            this.Close();
        }
    }
    
    public void delete(int id) throws Exception{
        try{
            this.Conect();
            PreparedStatement ps=this.getConnection().prepareStatement(""
                    + "UPDATE Sector SET activo=0 WHERE "
                    + "id=?");
            ps.setInt(1, id);
            ps.executeUpdate();
        }catch(Exception e){
            throw e;
        }finally{
            this.Close();
        }
    }    
    
    
}
