/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.judicialito.bean;

import com.judicialito.dao.ElementoDAO;
import com.judicialito.dao.SectorDAO;
import com.judicialito.model.Elemento;
import java.io.Serializable;
import java.util.ArrayList;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author jacobotapia
 */
@ManagedBean
@ViewScoped
public class ElementoBean implements Serializable{
    
    private static final long serialVersionUID = 1L;
    private Elemento elemento=new Elemento();
    private ArrayList<Elemento> elementos=new ArrayList<Elemento>();
    
    public void registar() throws Exception{
        ElementoDAO dao;
        try{
            dao=new ElementoDAO();
            dao.registrar(elemento);
            this.elemento.limpiar();
        }catch(Exception e){
            throw e;
        }
    }
    
    public void listar() throws Exception{
        ElementoDAO dao;
        SectorDAO daoS;
        try{
            dao=new ElementoDAO();
            daoS=new SectorDAO();
            this.elementos=dao.elementos();
            for(int i=0;i<elementos.size();i++){
                int id=elementos.get(i).getSectorID();
                elementos.get(i).setSector(daoS.getOneByID(id));
            }
        }catch(Exception e){
            throw e;
        }
    } 
    
    public void getOneByID(int id) throws Exception{
        ElementoDAO dao;
        try{
            dao=new ElementoDAO();
            Elemento aux=dao.getOneByID(id);
            if(aux!=null){
                this.elemento=aux;
            }
        }catch(Exception e){
            throw e;
        }
    }    

    public void update() throws Exception{
        ElementoDAO dao;
        try{
            dao=new ElementoDAO();
            dao.update(elemento);
            this.elemento.limpiar();
        }catch(Exception e){
            throw e;
        }
    }
    
    public void delete(int id) throws Exception{
        ElementoDAO dao;
        try{
            dao=new ElementoDAO();
            dao.delete(id);
        }catch(Exception e){
            throw e;
        }
    }            
          
    /**
     * @return the elemento
     */
    public Elemento getElemento() {
        return elemento;
    }

    /**
     * @param elemento the elemento to set
     */
    public void setElemento(Elemento elemento) {
        this.elemento = elemento;
    }

    /**
     * @return the elementos
     */
    public ArrayList<Elemento> getElementos() {
        return elementos;
    }

    /**
     * @param elementos the elementos to set
     */
    public void setElementos(ArrayList<Elemento> elementos) {
        this.elementos = elementos;
    }
}
