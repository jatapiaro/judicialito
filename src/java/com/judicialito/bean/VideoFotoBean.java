/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.judicialito.bean;

import com.judicialito.dao.FotosDAO;
import com.judicialito.dao.ObjetosDAO;
import com.judicialito.dao.VideosDAO;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author jacobotapia
 */
@ManagedBean
@ViewScoped
public class VideoFotoBean {
    public void verImagen(int id) throws Exception {
        FotosDAO dao;
        byte[] bytes = null;
        try {
            dao = new FotosDAO();
            bytes = dao.getFoto(id);
            HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
            response.getOutputStream().write(bytes);
            response.getOutputStream().close();
            FacesContext.getCurrentInstance().responseComplete();
        } catch (Exception e) {
            throw e;
        }
    }
    
    public void verVideo(int id) throws Exception {
        VideosDAO dao;
        byte[] bytes = null;
        try {
            dao = new VideosDAO();
            bytes = dao.getVideo(id);
            HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
            response.getOutputStream().write(bytes);
            response.getOutputStream().close();
            FacesContext.getCurrentInstance().responseComplete();
        } catch (Exception e) {
            throw e;
        }
    }
}
