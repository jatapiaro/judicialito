/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.judicialito.bean;


import java.io.IOException;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author jacobotapia
 */

@ManagedBean
@SessionScoped
public class RoutingBean implements Serializable{
    
    private static final long serialVersionUID = 1L;
    
    /*
    *FacesContext.getCurrentInstance().getApplication().getNavigationHandler().handleNavigation(FacesContext.getCurrentInstance(), null, "/views/paciente/List.xhtml");
    */
    
    public void infractores() throws IOException{
        FacesContext.getCurrentInstance().getExternalContext().redirect("/Judicialito/faces/views/infractores/List.xhtml");
    }
    public void patrullas() throws IOException{
        FacesContext.getCurrentInstance().getExternalContext().redirect("/Judicialito/faces/views/patrullas/List.xhtml");
    }
    public void sectores() throws IOException{
        FacesContext.getCurrentInstance().getExternalContext().redirect("/Judicialito/faces/views/sectores/List.xhtml");
    }
    public void elementos() throws IOException{
        FacesContext.getCurrentInstance().getExternalContext().redirect("/Judicialito/faces/views/elementos/List.xhtml");
    } 
    public void camaras() throws IOException{
        FacesContext.getCurrentInstance().getExternalContext().redirect("/Judicialito/faces/views/camaras/List.xhtml");
    } 
    public void reportesCreate() throws IOException {
        FacesContext.getCurrentInstance().getExternalContext().redirect("/Judicialito/faces/views/reportes/List.xhtml");
    }
    public void reportesView() throws IOException {
        FacesContext.getCurrentInstance().getExternalContext().redirect("/Judicialito/faces/views/reportes/Create.xhtml");
    }
    
}
