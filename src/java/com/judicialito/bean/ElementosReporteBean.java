/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.judicialito.bean;

import com.judicialito.dao.ElementoDAO;
import com.judicialito.dao.ElementosReporteDAO;
import com.judicialito.dao.PatrullaDAO;
import com.judicialito.model.Elemento;
import com.judicialito.model.ElementosReporte;
import com.judicialito.model.Patrulla;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;


/**
 *
 * @author jacobotapia
 */
@ManagedBean
@ViewScoped
public class ElementosReporteBean implements Serializable{
    private static final long serialVersionUID = 1L;
    
    private ElementosReporte elementoReporte=new ElementosReporte();
    
    public void registrar(int reporteID) throws Exception{
        this.elementoReporte.setReporteID(reporteID);
        ElementosReporteDAO dao;
        try{
            dao=new ElementosReporteDAO();
            dao.registrar(elementoReporte);
            this.elementoReporte.limpiar();
        }catch(Exception e){
            throw e;
        }
    }

    /**
     * @return the elementoReporte
     */
    public ElementosReporte getElementoReporte() {
        return elementoReporte;
    }

    /**
     * @param elementoReporte the elementoReporte to set
     */
    public void setElementoReporte(ElementosReporte elementoReporte) {
        this.elementoReporte = elementoReporte;
    }
    
    public void elementoInfo() throws Exception{
        ElementoDAO dao;
        try{
            dao=new ElementoDAO();
            Elemento el=dao.getOneByPlaca(this.elementoReporte.getElemento().getPlaca());
            if(el!=null){
                this.elementoReporte.setElemento(el);
            }else{
                el=new Elemento();
                this.elementoReporte.setElemento(el);
            }
        }catch(Exception e){
            throw e;
        }
    }
    
    public void patrullaInfo() throws Exception{
        PatrullaDAO dao;
        try{
            dao=new PatrullaDAO();
            Patrulla p=dao.getOneByPlacas(this.getElementoReporte().getPatrulla().getPlacas());
            if(p!=null){
                this.elementoReporte.setPatrulla(p);
            }else{
                this.elementoReporte.setPatrulla(new Patrulla());
            }
        }catch(Exception e){
            throw e;
        }
    }
    
    public void registrarParteInformativo() throws Exception{
        ElementosReporteDAO dao;
        try{
            dao=new ElementosReporteDAO();
            dao.registrarParteInformativo(elementoReporte);
            this.elementoReporte.limpiar();
        }catch(Exception e){
            throw e;
        }
    }
    
    public void findOne(int id) throws Exception{
        ElementosReporteDAO dao;
        try{
            dao=new ElementosReporteDAO();
            ElementosReporte el=dao.findOne(id);
            if(el!=null){
                this.elementoReporte=el;
            }
        }catch(Exception e){
            throw e;
        }        
    }
}
