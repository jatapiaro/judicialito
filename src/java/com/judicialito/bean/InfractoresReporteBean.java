/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.judicialito.bean;

import com.judicialito.dao.DireccionDAO;
import com.judicialito.dao.InfractorDAO;
import com.judicialito.dao.InfractoresReporteDAO;
import com.judicialito.model.Direccion;
import com.judicialito.model.Infractor;
import com.judicialito.model.InfractorReporte;
import java.io.Serializable;
import java.util.ArrayList;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author jacobotapia
 */
@ManagedBean
@ViewScoped
public class InfractoresReporteBean implements Serializable{
    
    private static final long serialVersionUID = 1L;
    private InfractorReporte infractorReporte=new InfractorReporte();
    private ArrayList<String> delitos;
    private String[] selectedDelitos;
    
    @PostConstruct
    public void init(){
        delitos=new ArrayList<String>();
        delitos.add("Homicidio Doloso");
        delitos.add("Homicidio Imprudente");
        delitos.add("Asesinato");
        delitos.add("Auxilio e inducción al suicidio");
        delitos.add("Aborto doloso");
        delitos.add("Aborto imprudente");
        delitos.add("Concepto de lesión");
        delitos.add("Consentimiento en las lesiones");
        delitos.add("Maltrato Singular");
        delitos.add("Particiàción en riña tumultaria");
        delitos.add("Tráfico ilegal de órganos");
        delitos.add("Detenciones ilegales y secuestros");
        delitos.add("Delitos de amenazas");
        delitos.add("Delitos de coacciones");
        delitos.add("Tortura y otros delitos contra la integridad moral");
        delitos.add("Trata de seres humanos");
        delitos.add("Agresiones sexuales");
        delitos.add("Abusos sexuales");
        delitos.add("Agresiones y abusos a menores de 13 años");
        delitos.add("Acoso sexual");
        delitos.add("Exhibiciones y provocación sexual");
        delitos.add("Allanamiento de morada");
        delitos.add("Robo");
        delitos.add("Extorsion");
        delitos.add("Estafa");
        delitos.add("Daños");
        delitos.add("Alzamiento de bienes");
        delitos.add("Tráfico de drogas");
        delitos.add("Conducir a exceso de velocidad");
        delitos.add("Conducir bajo influencia de tóxicos");
        delitos.add("Abandono de familia");
        delitos.add("Explotación de menores");
        delitos.add("Incumplimiento de deberes económicos");
        delitos.add("Falsificación de moneda");
        delitos.add("Tráfico de influencias");
        delitos.add("Encubrimiento");
        java.util.Collections.sort(delitos);
    }
    
    public void registrar(int id) throws Exception{
        InfractoresReporteDAO dao;
        for(String s:this.selectedDelitos){
            this.infractorReporte.getDelitos().add(s);
        }
        this.infractorReporte.setReporteID(id);
        try{
            dao=new InfractoresReporteDAO();
            dao.registrar(infractorReporte);
            this.infractorReporte.limpiar();
        }catch(Exception e){
            throw e;
        }
    }

    /**
     * @return the infractorReporte
     */
    public InfractorReporte getInfractorReporte() {
        return infractorReporte;
    }

    /**
     * @param infractorReporte the infractorReporte to set
     */
    public void setInfractorReporte(InfractorReporte infractorReporte) {
        this.infractorReporte = infractorReporte;
    }
    
    public void findOneByRfc() throws Exception{
        InfractorDAO dao;
        try{
            dao=new InfractorDAO();
            Infractor i=dao.getOneByRFC(this.infractorReporte.getInfractor().getRfc().toUpperCase());
            if(i!=null){
                this.infractorReporte.setInfractor(i);
            }else{
                this.infractorReporte.setInfractor(new Infractor());
            }
        }catch(Exception e){
            throw e;
        }
    }
    
    public void findDirection() throws Exception{
        DireccionDAO dao;
        try{
            dao=new DireccionDAO();
            int cp=this.infractorReporte.getDireccion().getCodigoPostal();
            Direccion d=dao.direccion(cp);
            if(d!=null){
                this.infractorReporte.setDireccion(d);
            }else{
                this.infractorReporte.setDireccion(new Direccion());
            }
        }catch(Exception e){
            throw e;
        }
    }

    /**
     * @return the selectedDelitos
     */
    public String[] getSelectedDelitos() {
        return selectedDelitos;
    }

    /**
     * @param selectedDelitos the selectedDelitos to set
     */
    public void setSelectedDelitos(String[] selectedDelitos) {
        this.selectedDelitos = selectedDelitos;
    }

    /**
     * @return the delitos
     */
    public ArrayList<String> getDelitos() {
        return delitos;
    }
    
    public void verImagen(int id) throws Exception {
        InfractoresReporteDAO dao;
        byte[] bytes = null;
        try {
            dao = new InfractoresReporteDAO();
            bytes = dao.getImage(id);
            HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
            response.getOutputStream().write(bytes);
            response.getOutputStream().close();
            FacesContext.getCurrentInstance().responseComplete();
        } catch (Exception e) {
            throw e;
        }
    }
    
}
