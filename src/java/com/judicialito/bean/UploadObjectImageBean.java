/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.judicialito.bean;

import com.judicialito.dao.FotosDAO;
import com.judicialito.dao.InfractoresReporteDAO;
import com.judicialito.dao.ObjetosDAO;
import com.judicialito.dao.VideosDAO;
import com.judicialito.model.Camara;
import com.judicialito.model.Foto;
import com.judicialito.model.Video;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author jacobotapia
 */
@ManagedBean
@SessionScoped
public class UploadObjectImageBean {
    
    private UploadedFile file;
    private Foto foto=new Foto();
    private Video video=new Video();
    
    public void upload(int id) throws Exception{
        ObjetosDAO dao;
        try{
            dao=new ObjetosDAO();
            dao.regFile(file, id);
            FacesMessage message=new FacesMessage("La foto del objeto fue subida exitosamente",file.getFileName()+" fue subido");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }catch(Exception e){
            FacesMessage message=new FacesMessage("Error de conexión");
            FacesContext.getCurrentInstance().addMessage(null, message);
            throw e;
        }
    }
    
    public void uploadInfractor(int id) throws Exception{
        InfractoresReporteDAO dao;
        try{
            dao=new InfractoresReporteDAO();
            dao.insertImage(file, id);
            FacesMessage message = new FacesMessage("La foto del detenido fue subida exitosamente", file.getFileName() + " fue subido");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }catch(Exception e){
            FacesMessage message = new FacesMessage("Error de conexión");
            FacesContext.getCurrentInstance().addMessage(null, message);
            throw e;
        }
    }
    
    public void uploadCaseFoto(int reporteID) throws Exception{
        FotosDAO dao;
        getFoto().setReporteID(reporteID);
        try{
            dao=new FotosDAO();
            dao.registrar(file, getFoto());
            FacesMessage message = new FacesMessage("La foto fue subida exitosamente", file.getFileName() + " fue subido");
            FacesContext.getCurrentInstance().addMessage(null, message);
            this.getFoto().limpiar();
        }catch(Exception e){
            FacesMessage message = new FacesMessage("Error de conexión");
            FacesContext.getCurrentInstance().addMessage(null, message);
            throw e;
        }
    }
    
    
    public void uploadCaseVideo(int reporteID) throws Exception {
        VideosDAO dao;
        video.setReporteID(reporteID);
        try {
            dao = new VideosDAO();
            dao.registrar(file, video);
            FacesMessage message = new FacesMessage("El video fue subida exitosamente", file.getFileName() + " fue subido");
            FacesContext.getCurrentInstance().addMessage(null, message);
            this.video.limpiar();
        } catch (Exception e) {
            FacesMessage message = new FacesMessage("Error de conexión");
            FacesContext.getCurrentInstance().addMessage(null, message);
            throw e;
        }
    }
    
    

    /**
     * @return the file
     */
    public UploadedFile getFile() {
        return file;
    }

    /**
     * @param file the file to set
     */
    public void setFile(UploadedFile file) {
        this.file = file;
    }

    /**
     * @return the video
     */
    public Video getVideo() {
        return video;
    }

    /**
     * @param video the video to set
     */
    public void setVideo(Video video) {
        this.video = video;
    }

    /**
     * @return the foto
     */
    public Foto getFoto() {
        return foto;
    }

    /**
     * @param foto the foto to set
     */
    public void setFoto(Foto foto) {
        this.foto = foto;
    }

}
