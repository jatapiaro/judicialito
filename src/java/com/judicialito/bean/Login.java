/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.judicialito.bean;

import com.judicialito.dao.ElementoDAO;
import com.judicialito.model.Elemento;
import java.io.IOException;
import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author jacobotapia
 */
@ManagedBean
@SessionScoped
public class Login implements Serializable{
    private static final long serialVersionUID = 1L;
    private String psw,placa;
    private Elemento e=null;
    
    public void login() throws Exception{
        ElementoDAO dao;
        try{
            dao=new ElementoDAO();
            Elemento aux=dao.login(placa,psw);
            if(aux!=null){
                this.e=aux;
                FacesContext.getCurrentInstance().getExternalContext().redirect("/Judicialito/faces/views/reportes/List.xhtml");               
            }else{
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "No tienes acceso al sistema."));                  
            }
        }catch(Exception e){
            throw e;
        }
    }
    
    public void grantAcces() throws Exception {
        if (this.e == null) {
            try{
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Necesitas ingresar al sistema."));            
                FacesContext.getCurrentInstance().getApplication().getNavigationHandler().handleNavigation(FacesContext.getCurrentInstance(), null, "/index.xhtml");   
            }catch(Exception e){
                throw e;
            }
        }
    }    

    /**
     * @return the e
     */
    public Elemento getE() {
        return e;
    }

    /**
     * @param e the e to set
     */
    public void setE(Elemento e) {
        this.e = e;
    }


    /**
     * @return the psw
     */
    public String getPsw() {
        return psw;
    }

    /**
     * @param psw the psw to set
     */
    public void setPsw(String psw) {
        this.psw = psw;
    }

    /**
     * @return the placa
     */
    public String getPlaca() {
        return placa;
    }

    /**
     * @param placa the placa to set
     */
    public void setPlaca(String placa) {
        this.placa = placa;
    }
    
    public void logout() throws IOException {
        this.setE(null);
        FacesContext.getCurrentInstance().getExternalContext().redirect("/Judicialito/faces/index.xhtml");  
    }
}
