/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.judicialito.bean;

import com.judicialito.dao.CamaraDAO;
import com.judicialito.dao.DireccionDAO;
import com.judicialito.dao.ElementosReporteDAO;
import com.judicialito.dao.ReporteDAO;
import com.judicialito.dao.SectorDAO;
import com.judicialito.model.Camara;
import com.judicialito.model.Reporte;
import com.judicialito.model.Denunciante;
import com.judicialito.model.Direccion;
import com.judicialito.model.ElementosReporte;
import com.judicialito.model.Infractor;
import com.judicialito.model.Sector;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

/**
 *
 * @author jacobotapia
 */
@ManagedBean
@ViewScoped
public class ReporteBean implements Serializable{
    
    private static final long serialVersionUID = 1L;
    private Reporte reporte=new Reporte();
    private Reporte consulta=new Reporte();
    private ArrayList<Reporte> reportes=new ArrayList<Reporte>();
    private Date date=new Date();
    private int reporteID;
    
    
    public void registrar() throws Exception{
        ReporteDAO dao;
        try{
            dao=new ReporteDAO();
            reporteID=dao.registrar(reporte);
            if(reporteID==0){
                reporteID=1200;
            }
        }catch(Exception e){
            throw e;
        }

        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        Map<String, Object> sessionMap = externalContext.getSessionMap();
        sessionMap.put("id", reporteID);
        //FacesContext.getCurrentInstance().getApplication().getNavigationHandler().handleNavigation(FacesContext.getCurrentInstance(), null, "/views/reportes/View.xhtml");
        FacesContext.getCurrentInstance().getExternalContext().redirect("/Judicialito/faces/views/reportes/View.xhtml");
    }
    
    public void listar() throws Exception{
        ReporteDAO dao;
        try{
            dao=new ReporteDAO();
            this.reportes=dao.listar();
        }catch(Exception e){
            throw e;
        }
    }

    public void findOneAfterRegistration() throws Exception{
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        Map<String, Object> sessionMap = externalContext.getSessionMap();
        int id = (int) sessionMap.get("id");
        ReporteDAO dao;
        try{
            dao=new ReporteDAO();
            Reporte r=dao.getOneByID(id);
            if(r!=null){
                this.consulta=r;
            }
        }catch(Exception e){
            throw e;
        }
    }
    
    public void redirectReportePage(int id) throws Exception {
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        Map<String, Object> sessionMap = externalContext.getSessionMap();
        sessionMap.put("id", id);
        //FacesContext.getCurrentInstance().getApplication().getNavigationHandler().handleNavigation(FacesContext.getCurrentInstance(), null, "/views/reportes/View.xhtml");
        FacesContext.getCurrentInstance().getExternalContext().redirect("/Judicialito/faces/views/reportes/View.xhtml");        
    }    

    /**
     * @return the reporte
     */
    public Reporte getReporte() {
        return reporte;
    }

    /**
     * @param reporte the reporte to set
     */
    public void setReporte(Reporte reporte) {
        this.reporte = reporte;
    } 

    /**
     * @return the reportes
     */
    public ArrayList<Reporte> getReportes() {
        return reportes;
    }

    /**
     * @param reportes the reportes to set
     */
    public void setReportes(ArrayList<Reporte> reportes) {
        this.reportes = reportes;
    }
    
    public void validarDireccion() throws Exception{
        DireccionDAO dao;
        try{
            dao=new DireccionDAO();
            int id=this.reporte.getDireccion().getCodigoPostal();
            Direccion d=dao.direccion(id);
            if(d!=null){
                this.reporte.setDireccion(d);
            }else{
                d=new Direccion();
                d.setColonia("No encontrado");
                d.setCodigoPostal(0);
                d.setEstado("No encontrado");
                d.setMunicipio("No encontrado");
                this.reporte.setDireccion(d);
            }
        }catch(Exception e){
            throw e;
        }
    }
    
    public Date currentMinDate(){
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        date.setHours(0);
        date.setMinutes(0);
        date.setSeconds(0);
        return date;
    }
    public Date currentMaxDate(){
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        date.setHours(23);
        date.setMinutes(59);
        date.setSeconds(59);
        return date;
    }      

    /**
     * @return the date
     */
    public Date getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(Date date) {
        this.date = date;
    }
    
    
    public void validarDireccion2() throws Exception{
        DireccionDAO dao;
        try{
            dao=new DireccionDAO();
            int id=this.reporte.getDenunciante().getDireccion().getCodigoPostal();
            Direccion d=dao.direccion(id);
            if(d!=null){
                this.reporte.getDenunciante().setDireccion(d);
            }else{
                d=new Direccion();
                d.setColonia("No encontrado");
                d.setCodigoPostal(0);
                d.setEstado("No encontrado");
                d.setMunicipio("No encontrado");
                this.reporte.getDenunciante().setDireccion(d);
            }
        }catch(Exception e){
            throw e;
        }
    } 
    
    @Override
    public String toString(){
        return this.reporte.getDenunciante().getFirma();
    }
    
    public void getCamera() throws Exception{
        CamaraDAO dao;
        try{
            dao=new CamaraDAO();
            Camara c=dao.getOneByID(this.getReporte().getCamara().getId());
            if(c!=null){
                this.reporte.setCamara(c);
            }
        }catch(Exception e){
            throw e;
        }
    }
    
    public void setSector() throws Exception{
        SectorDAO dao;
        try{
            dao=new SectorDAO();
            Sector s=dao.getOneByID(this.reporte.getSectorID());
            if(s!=null){
                this.reporte.setSector(s);
            }
        }catch(Exception e){
            throw e;
        }
    }

    /**
     * @return the reporteID
     */
    public int getReporteID() {
        return reporteID;
    }

    /**
     * @param reporteID the reporteID to set
     */
    public void setReporteID(int reporteID) {
        this.reporteID = reporteID;
    }
    
    public void prepareNewReport(){
        Reporte d=new Reporte();
        if(d!=null){
            this.reporte=d;
        }
    }

    /**
     * @return the consulta
     */
    public Reporte getConsulta() {
        return consulta;
    }

    /**
     * @param consulta the consulta to set
     */
    public void setConsulta(Reporte consulta) {
        this.consulta = consulta;
    }
    
    public boolean canMakeReport(ElementosReporte el,int id, int nivelAcceso){
        if(!el.isContesto() && (el.getElemento().getId()==id)){
            return true;
        }else{
            return false;
        }
    }
    
    public void listarInfractor(int idInfractor) throws Exception{
        ReporteDAO dao;
        try{
            dao=new ReporteDAO();
            this.reportes=dao.listar(idInfractor);
        }catch(Exception e){
            throw e;
        }
    }
    
    
}
