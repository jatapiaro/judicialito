/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.judicialito.bean;

import com.judicialito.dao.ObjetosDAO;
import com.judicialito.model.Objeto;
import java.io.Serializable;
import java.util.ArrayList;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author jacobotapia
 */
@ManagedBean
@ViewScoped
public class ObjetosBean implements Serializable{
    
    private static final long serialVersionUID = 1L;
    private Objeto objeto=new Objeto();
    private ArrayList<Objeto> objetos=new ArrayList<Objeto>();
    
    public void registrar(int reporteID) throws Exception{
        this.objeto.setIdReporte(reporteID);
        ObjetosDAO dao;
        try{
            dao=new ObjetosDAO();
            dao.registrar(objeto);
            objeto.limpiar();
        }catch(Exception e){
            throw e;
        }
    }
    
    public void listar() throws Exception{
        ObjetosDAO dao;
        try{
            dao=new ObjetosDAO();
            this.objetos=dao.listar(67);
        }catch(Exception e){
            throw e;
        }
    }    
       

    /**
     * @return the objeto
     */
    public Objeto getObjeto() {
        return objeto;
    }

    /**
     * @param objeto the objeto to set
     */
    public void setObjeto(Objeto objeto) {
        this.objeto = objeto;
    }

    /**
     * @return the objetos
     */
    public ArrayList<Objeto> getObjetos() {
        return objetos;
    }

    /**
     * @param objetos the objetos to set
     */
    public void setObjetos(ArrayList<Objeto> objetos) {
        this.objetos = objetos;
    }
    
    public void verImagen(int id) throws Exception{
        ObjetosDAO dao;
        byte[] bytes=null;
        try{
            dao=new ObjetosDAO();
            bytes=dao.findOne(id);
            HttpServletResponse response=(HttpServletResponse)FacesContext.getCurrentInstance().getExternalContext().getResponse();
            response.getOutputStream().write(bytes);
            response.getOutputStream().close();
            FacesContext.getCurrentInstance().responseComplete();
        }catch(Exception e){
            throw e;
        }
    }
}
