/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.judicialito.bean;

import com.judicialito.dao.InfractorDAO;
import com.judicialito.dao.ReporteDAO;
import com.judicialito.model.Infractor;
import com.judicialito.model.Reporte;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

/**
 *
 * @author jacobotapia
 */

@ManagedBean
@ViewScoped
public class InfractorBean implements Serializable{
    
    private static final long serialVersionUID = 1L;
    private Infractor infractor=new Infractor();
    private ArrayList<Infractor> infractores=new ArrayList<Infractor>();
    
    public void registrar() throws Exception{
        InfractorDAO dao;
        try{
            dao=new InfractorDAO();
            dao.registrar(infractor);
            this.infractor.limpiar();
        }catch(Exception e){
            throw e;
        }
    }
    
    public void listar() throws Exception{
        InfractorDAO dao;
        try{
            dao=new InfractorDAO();
            this.infractores=dao.infractores();
        }catch(Exception e){
            throw e;
        }        
    }
    
    public void update() throws Exception{
        InfractorDAO dao;
        try{
            dao=new InfractorDAO();
            dao.update(infractor);
            infractor.limpiar();
        }catch(Exception e){
            throw e;
        }    
    }
    
    public void getOneByID(int id) throws Exception{
        InfractorDAO dao;
        try{
            dao=new InfractorDAO();
            Infractor aux=dao.getOneByID(id);
            if(aux!=null){
                this.infractor=aux;
            }
        }catch(Exception e){
            throw e;
        }    
    }    

    /**
     * @return the infractor
     */
    public Infractor getInfractor() {
        return infractor;
    }

    /**
     * @param infractor the infractor to set
     */
    public void setInfractor(Infractor infractor) {
        this.infractor = infractor;
    }

    /**
     * @return the infractores
     */
    public ArrayList<Infractor> getInfractores() {
        return infractores;
    }

    /**
     * @param infractores the infractores to set
     */
    public void setInfractores(ArrayList<Infractor> infractores) {
        this.infractores = infractores;
    }
    
    public void infractorPage(int infractorID) throws IOException{
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        Map<String, Object> sessionMap = externalContext.getSessionMap();
        sessionMap.put("infractorID", infractorID);
        //FacesContext.getCurrentInstance().getApplication().getNavigationHandler().handleNavigation(FacesContext.getCurrentInstance(), null, "/views/reportes/View.xhtml");
        FacesContext.getCurrentInstance().getExternalContext().redirect("/Judicialito/faces/views/infractores/View.xhtml");
    }
    
    
    public void findOne() throws Exception {
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        Map<String, Object> sessionMap = externalContext.getSessionMap();
        int id = (int) sessionMap.get("infractorID");
        InfractorDAO dao;
        try {
            dao = new InfractorDAO();
            Infractor r = dao.getOneByID(id);
            if (r != null) {
                this.infractor=r;
            }
        } catch (Exception e) {
            throw e;
        }
    }
}
