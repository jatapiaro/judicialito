/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.judicialito.bean;

import com.judicialito.dao.PatrullaDAO;
import com.judicialito.dao.SectorDAO;
import com.judicialito.model.Patrulla;
import java.io.Serializable;
import java.util.ArrayList;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author jacobotapia
 */
@ManagedBean
@ViewScoped
public class PatrullaBean implements Serializable{
    private static final long serialVersionUID = 1L;
    private Patrulla patrulla=new Patrulla();
    private ArrayList<Patrulla> patrullas=new ArrayList<Patrulla>();
    
    public void registrar() throws Exception{
        PatrullaDAO dao;
        try{
            dao=new PatrullaDAO();
            dao.registrar(patrulla);
            patrulla.limpiar();
        }catch(Exception e){
            throw e;
        }
    }
    
    public void listar() throws Exception{
        PatrullaDAO dao;
        SectorDAO daoS;
        try{
            dao=new PatrullaDAO();
            daoS=new SectorDAO();
            this.patrullas=dao.patrullas();
            for(int i=0;i<patrullas.size();i++){
                int id=patrullas.get(i).getSectorID();
                patrullas.get(i).setSector(daoS.getOneByID(id));
            }
        }catch(Exception e){
            throw e;
        }
    }  
    
    public void getOneByID(int id) throws Exception{
        PatrullaDAO dao;
        try{
            dao=new PatrullaDAO();
            Patrulla aux=dao.getOneById(id);
            if(aux!=null){
                this.patrulla=aux;
            }
        }catch(Exception e){
            throw e;
        }
    }  
    
    public void update() throws Exception{
        PatrullaDAO dao;
        try{
            dao=new PatrullaDAO();
            dao.update(patrulla);
            patrulla.limpiar();
        }catch(Exception e){
            throw e;
        }
    }

    public void delete(int id) throws Exception{
        PatrullaDAO dao;
        try{
            dao=new PatrullaDAO();
            dao.delete(id);
        }catch(Exception e){
            throw e;
        }
    }    
    
    
    /**
     * @return the patrulla
     */
    public Patrulla getPatrulla() {
        return patrulla;
    }

    /**
     * @param patrulla the patrulla to set
     */
    public void setPatrulla(Patrulla patrulla) {
        this.patrulla = patrulla;
    }

    /**
     * @return the patrullas
     */
    public ArrayList<Patrulla> getPatrullas() {
        return patrullas;
    }

    /**
     * @param patrullas the patrullas to set
     */
    public void setPatrullas(ArrayList<Patrulla> patrullas) {
        this.patrullas = patrullas;
    }
    
}
