/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.judicialito.bean;

import com.judicialito.dao.DireccionDAO;
import com.judicialito.dao.TestigosDAO;
import com.judicialito.model.Direccion;
import com.judicialito.model.Testigo;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author jacobotapia
 */
@ManagedBean
@ViewScoped
public class TestigosBean {
    private static final long serialVersionUID = 1L;
    private Testigo testigo=new Testigo();
    
    public void registrar(int id) throws Exception{
        TestigosDAO dao;
        this.testigo.setReporteID(id);
        try{
            dao=new TestigosDAO();
            dao.registrar(testigo);
            this.testigo.limpiar();
            this.testigo.setTestimonio("");
        }catch(Exception e){
            throw e;
        }
    }

    /**
     * @return the testigo
     */
    public Testigo getTestigo() {
        return testigo;
    }

    /**
     * @param testigo the testigo to set
     */
    public void setTestigo(Testigo testigo) {
        this.testigo = testigo;
    }
    
    public void findDireccion() throws Exception{
        DireccionDAO dao;
        try{
            dao=new DireccionDAO();
            Direccion d=dao.direccion(this.testigo.getDireccion().getCodigoPostal());
            if(d!=null){
                this.testigo.setDireccion(d);
            }else{
                this.testigo.setDireccion(new Direccion());
            }
        }catch(Exception e){
            throw e;
        }
    }
}
