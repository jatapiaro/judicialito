/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.judicialito.bean;

import com.judicialito.dao.CamaraDAO;
import com.judicialito.dao.DireccionDAO;
import com.judicialito.model.Camara;
import com.judicialito.model.Direccion;
import java.io.Serializable;
import java.util.ArrayList;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author jacobotapia
 */
@ManagedBean
@ViewScoped
public class CamaraBean implements Serializable{
    private static final long serialVersionUID = 1L;
    private Camara camara=new Camara();
    private ArrayList<Camara> camaras=new ArrayList<Camara>();
    
    public void registrar() throws Exception{
        CamaraDAO dao;
        try{
            dao=new CamaraDAO();
            dao.registrar(camara);
            this.camara.limpiar();
        }catch(Exception e){
            throw e;
        }
    }
    
    
    public void listar() throws Exception{
        CamaraDAO dao;
        try{
            dao=new CamaraDAO();
            this.camaras=dao.camaras();
        }catch(Exception e){
            throw e;
        }
    }  
    
    public void findOne(int id) throws Exception{
        CamaraDAO dao;
        try{
            dao=new CamaraDAO();
            Camara c=dao.getOneByID(id);
            if(c!=null){
                this.camara=c;
            }
        }catch(Exception e){
            throw e;
        }
    }    


    public void update() throws Exception{
        CamaraDAO dao;
        try{
            dao=new CamaraDAO();
            dao.update(camara);
            this.camara.limpiar();
        }catch(Exception e){
            throw e;
        }
    } 
    
    public void delete(int id) throws Exception{
        CamaraDAO dao;
        try{
            dao=new CamaraDAO();
            dao.delete(id);
        }catch(Exception e){
            throw e;
        }
    }    

    /**
     * @return the camara
     */
    public Camara getCamara() {
        return camara;
    }

    /**
     * @param camara the camara to set
     */
    public void setCamara(Camara camara) {
        this.camara = camara;
    }

    /**
     * @return the camaras
     */
    public ArrayList<Camara> getCamaras() {
        return camaras;
    }

    /**
     * @param camaras the camaras to set
     */
    public void setCamaras(ArrayList<Camara> camaras) {
        this.camaras = camaras;
    }
    
    
    public void validarDireccion() throws Exception{
        DireccionDAO dao;
        try{
            dao=new DireccionDAO();
            Direccion d=dao.direccion(this.camara.getDireccion().getCodigoPostal());
            if(d!=null){
                this.camara.setDireccion(d);
            }else{
                d=new Direccion();
                this.camara.setDireccion(d);
            }
        }catch(Exception e){
            throw e;
        }
    }
    
}
