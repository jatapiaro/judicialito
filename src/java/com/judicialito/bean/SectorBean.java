/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.judicialito.bean;

import com.judicialito.dao.SectorDAO;
import com.judicialito.model.Sector;
import java.io.Serializable;
import java.util.ArrayList;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author jacobotapia
 */

@ManagedBean
@ViewScoped
public class SectorBean implements Serializable{
    
    private static final long serialVersionUID = 1L;
    private Sector sector=new Sector();
    private ArrayList<Sector> sectores=new ArrayList<Sector>();
    
    public void registrar() throws Exception{
        SectorDAO dao;
        try{
            dao=new SectorDAO();
            dao.registrar(sector);
            this.sector.limpiar();
        }catch(Exception e){
            throw e;
        }
    }
    
    public void listar() throws Exception{
        SectorDAO dao;
        try{
            dao=new SectorDAO();
            this.sectores=dao.sectores();
        }catch(Exception e){
            throw e;
        }
    } 
    
    
    public void getOneByID(int id) throws Exception{
        SectorDAO dao;
        try{
            dao=new SectorDAO();
            Sector aux=dao.getOneByID(id);
            if(aux!=null){
                this.sector=aux;
            }
        }catch(Exception e){
            throw e;
        }
    }     
    
    public void update() throws Exception{
        SectorDAO dao;
        try{
            dao=new SectorDAO();
            dao.update(sector);
            this.sector.limpiar();
        }catch(Exception e){
            throw e;
        }
    } 
    
    public void delete(int id) throws Exception{
        SectorDAO dao;
        try{
            dao=new SectorDAO();
            dao.delete(id);
        }catch(Exception e){
            throw e;
        }
    }     
    
    /**
     * @return the sector
     */
    public Sector getSector() {
        return sector;
    }

    /**
     * @param sector the sector to set
     */
    public void setSector(Sector sector) {
        this.sector = sector;
    }

    /**
     * @return the sectores
     */
    public ArrayList<Sector> getSectores() {
        return sectores;
    }

    /**
     * @param sectores the sectores to set
     */
    public void setSectores(ArrayList<Sector> sectores) {
        this.sectores = sectores;
    }
    
}
