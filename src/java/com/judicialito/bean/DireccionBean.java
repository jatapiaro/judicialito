/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.judicialito.bean;

import com.judicialito.dao.DireccionDAO;
import com.judicialito.model.Direccion;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author jacobotapia
 */
@ManagedBean
@ViewScoped
public class DireccionBean implements Serializable{
    
    private static final long serialVersionUID = 1L;
    private Direccion direccionReporte=new Direccion();
    private int clicked;
    
    public void validarDireccion() throws Exception {
        DireccionDAO dao;
        
        try {
            dao = new DireccionDAO();
            Direccion dir = dao.direccion(clicked);
            if (dir != null) {
                this.direccionReporte=dir;
            }
        } catch (Exception e) {
            throw e;
        }
    }    


    public Direccion getDireccionReporte() {
        return direccionReporte;
    }


    public void setDireccionReporte(Direccion direccionReporte) {
        this.direccionReporte = direccionReporte;
    }

    /**
     * @return the clicked
     */
    public int getClicked() {
        return clicked;
    }

    /**
     * @param clicked the clicked to set
     */
    public void setClicked(int clicked) {
        this.clicked = clicked;
    }
    
    
}
