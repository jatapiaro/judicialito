/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.judicialito.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jacobotapia
 */
public class InfractorReporte {
    
    private int id,reporteID;
    private Infractor infractor;
    private Direccion direccion;
    private boolean hasImage;
    private ArrayList<String> delitos;

    public InfractorReporte(){
        this.infractor=new Infractor();
        this.direccion=new Direccion();
        this.delitos=new ArrayList<String>();
    }
    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the reporteID
     */
    public int getReporteID() {
        return reporteID;
    }

    /**
     * @param reporteID the reporteID to set
     */
    public void setReporteID(int reporteID) {
        this.reporteID = reporteID;
    }

    /**
     * @return the infractor
     */
    public Infractor getInfractor() {
        return infractor;
    }

    /**
     * @param infractor the infractor to set
     */
    public void setInfractor(Infractor infractor) {
        this.infractor = infractor;
    }

    /**
     * @return the direccion
     */
    public Direccion getDireccion() {
        return direccion;
    }

    /**
     * @param direccion the direccion to set
     */
    public void setDireccion(Direccion direccion) {
        this.direccion = direccion;
    }

    /**
     * @return the hasImage
     */
    public boolean isHasImage() {
        return hasImage;
    }

    /**
     * @param hasImage the hasImage to set
     */
    public void setHasImage(boolean hasImage) {
        this.hasImage = hasImage;
    }
    
    public String delitosString(){
        String s="";
        for(int i=0;i<this.getDelitos().size();i++){
            s+=this.getDelitos().get(i)+",";
        }
        return s;
    }
    
    public void limpiar(){
        this.getDelitos().clear();
        this.infractor=new Infractor();
        this.direccion=new Direccion();
    }

    /**
     * @return the delitos
     */
    public ArrayList<String> getDelitos() {
        return delitos;
    }

    /**
     * @param delitos the delitos to set
     */
    public void setDelitos(ArrayList<String> delitos) {
        this.delitos = delitos;
    }
    
}
