/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.judicialito.model;

/**
 *
 * @author jacobotapia
 */
public class ElementosReporte {
    
    private int id,reporteID;
    private Elemento elemento;
    private Patrulla patrulla;
    private String parteInformativo;
    private boolean contesto;
    
    public ElementosReporte(){
        patrulla=new Patrulla();
        elemento=new Elemento();
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the reporteID
     */
    public int getReporteID() {
        return reporteID;
    }

    /**
     * @param reporteID the reporteID to set
     */
    public void setReporteID(int reporteID) {
        this.reporteID = reporteID;
    }

    /**
     * @return the elemento
     */
    public Elemento getElemento() {
        return elemento;
    }

    /**
     * @param elemento the elemento to set
     */
    public void setElemento(Elemento elemento) {
        this.elemento = elemento;
    }

    /**
     * @return the patrulla
     */
    public Patrulla getPatrulla() {
        return patrulla;
    }

    /**
     * @param patrulla the patrulla to set
     */
    public void setPatrulla(Patrulla patrulla) {
        this.patrulla = patrulla;
    }

    /**
     * @return the parteInformativo
     */
    public String getParteInformativo() {
        return parteInformativo;
    }

    /**
     * @param parteInformativo the parteInformativo to set
     */
    public void setParteInformativo(String parteInformativo) {
        this.parteInformativo = parteInformativo;
    }

    /**
     * @return the contesto
     */
    public boolean isContesto() {
        return contesto;
    }

    /**
     * @param contesto the contesto to set
     */
    public void setContesto(boolean contesto) {
        this.contesto = contesto;
    }
    
    public void limpiar(){
        patrulla=new Patrulla();
        elemento=new Elemento();
    }
}
