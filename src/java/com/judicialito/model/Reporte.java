/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.judicialito.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

/**
 *
 * @author jacobotapia
 */

public class Reporte {
   
    
    /*
    *String uniqueID = UUID.randomUUID().toString();
    */
    private int id,sectorID,camaraID;
    private String titulo,hecho,folio;
    private Direccion direccion;
    private Denunciante denunciante;
    private Date fechaHora;
    private Camara camara;
    private Sector sector;
    private Timestamp timestamp;
    private String timeSearch;
    private ArrayList<ElementosReporte> elementosReporte;
    private ArrayList<Objeto> objetos;
    private ArrayList<InfractorReporte> infractores;
    private ArrayList<Testigo> testigos;
    private ArrayList<Foto> fotos;
    private ArrayList<Video> videos;
    
    public Reporte(){
        this.folio=UUID.randomUUID().toString();
        this.direccion=new Direccion();
        this.denunciante=new Denunciante();
        this.camara=new Camara();
        this.sector=new Sector();
        this.elementosReporte=new ArrayList<ElementosReporte>();
        this.objetos=new ArrayList<Objeto>();
        this.infractores=new ArrayList<InfractorReporte>();
        this.testigos=new ArrayList<Testigo>();
        this.fotos=new ArrayList<Foto>();
        this.videos=new ArrayList<Video>();
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the sectorID
     */
    public int getSectorID() {
        return sectorID;
    }

    /**
     * @param sectorID the sectorID to set
     */
    public void setSectorID(int sectorID) {
        this.sectorID = sectorID;
    }

    /**
     * @return the titulo
     */
    public String getTitulo() {
        return titulo;
    }

    /**
     * @param titulo the titulo to set
     */
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    /**
     * @return the hecho
     */
    public String getHecho() {
        return hecho;
    }

    /**
     * @param hecho the hecho to set
     */
    public void setHecho(String hecho) {
        this.hecho = hecho;
    }

    /**
     * @return the direccion
     */
    public Direccion getDireccion() {
        return direccion;
    }

    /**
     * @param direccion the direccion to set
     */
    public void setDireccion(Direccion direccion) {
        this.direccion = direccion;
    }


    /**
     * @return the folio
     */
    public String getFolio() {
        return folio;
    }

    /**
     * @param folio the folio to set
     */
    public void setFolio(String folio) {
        this.folio = folio;
    }
    
    public void limpiar(){
        this.folio=this.titulo=this.hecho="";
        this.denunciante.limpiar();
        this.sectorID=1;
        this.direccion.limpiar();
    }

    /**
     * @return the denunciante
     */
    public Denunciante getDenunciante() {
        return denunciante;
    }

    /**
     * @param denunciante the denunciante to set
     */
    public void setDenunciante(Denunciante denunciante) {
        this.denunciante = denunciante;
    }

    /**
     * @return the fechaHora
     */
    public Date getFechaHora() {
        return fechaHora;
    }

    /**
     * @param fechaHora the fechaHora to set
     */
    public void setFechaHora(Date fechaHora) {
        this.fechaHora = fechaHora;
    }

    /**
     * @return the camaraID
     */
    public int getCamaraID() {
        return camaraID;
    }

    /**
     * @param camaraID the camaraID to set
     */
    public void setCamaraID(int camaraID) {
        this.camaraID = camaraID;
    }

    /**
     * @return the camara
     */
    public Camara getCamara() {
        return camara;
    }

    /**
     * @param camara the camara to set
     */
    public void setCamara(Camara camara) {
        this.camara = camara;
    }

    /**
     * @return the sector
     */
    public Sector getSector() {
        return sector;
    }

    /**
     * @param sector the sector to set
     */
    public void setSector(Sector sector) {
        this.sector = sector;
    }

    /**
     * @return the timestamp
     */
    public Timestamp getTimestamp() {
        return timestamp;
    }

    /**
     * @param timestamp the timestamp to set
     */
    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
        this.timeSearch=new SimpleDateFormat("dd/MM/yyyy HH:mm").format(this.timestamp);
    }

    /**
     * @return the timeSearch
     */
    public String getTimeSearch() {
        return timeSearch;
    }

    /**
     * @param timeSearch the timeSearch to set
     */
    public void setTimeSearch(String timeSearch) {
        this.timeSearch = timeSearch;
    }

    /**
     * @return the elementosReporte
     */
    public ArrayList<ElementosReporte> getElementosReporte() {
        return elementosReporte;
    }

    /**
     * @param elementosReporte the elementosReporte to set
     */
    public void setElementosReporte(ArrayList<ElementosReporte> elementosReporte) {
        this.elementosReporte = elementosReporte;
    }

    /**
     * @return the objetos
     */
    public ArrayList<Objeto> getObjetos() {
        return objetos;
    }

    /**
     * @param objetos the objetos to set
     */
    public void setObjetos(ArrayList<Objeto> objetos) {
        this.objetos = objetos;
    }

    /**
     * @return the infractores
     */
    public ArrayList<InfractorReporte> getInfractores() {
        return infractores;
    }

    /**
     * @param infractores the infractores to set
     */
    public void setInfractores(ArrayList<InfractorReporte> infractores) {
        this.infractores = infractores;
    }

    /**
     * @return the testigos
     */
    public ArrayList<Testigo> getTestigos() {
        return testigos;
    }

    /**
     * @param testigos the testigos to set
     */
    public void setTestigos(ArrayList<Testigo> testigos) {
        this.testigos = testigos;
    }

    /**
     * @return the fotos
     */
    public ArrayList<Foto> getFotos() {
        return fotos;
    }

    /**
     * @param fotos the fotos to set
     */
    public void setFotos(ArrayList<Foto> fotos) {
        this.fotos = fotos;
    }

    /**
     * @return the videos
     */
    public ArrayList<Video> getVideos() {
        return videos;
    }

    /**
     * @param videos the videos to set
     */
    public void setVideos(ArrayList<Video> videos) {
        this.videos = videos;
    }
    
}
