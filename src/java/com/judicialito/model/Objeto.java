/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.judicialito.model;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.UploadedFile;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author jacobotapia
 */
public class Objeto {
    private int id;
    private int idReporte;
    private String nombre;
    private String descripcion;
    private UploadedFile imagen;
    private byte[] imageInBytes;
    private boolean hasImage;
    
    

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the idReporte
     */
    public int getIdReporte() {
        return idReporte;
    }

    /**
     * @param idReporte the idReporte to set
     */
    public void setIdReporte(int idReporte) {
        this.idReporte = idReporte;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    public void limpiar(){
        this.nombre=this.descripcion="";
    }

    /**
     * @return the imagen
     */
    public UploadedFile getImagen() {
        return imagen;
    }

    /**
     * @param imagen the imagen to set
     */
    public void setImagen(UploadedFile imagen) {
        this.imagen = imagen;
    }

    /**
     * @return the downloadedFile
     */
    public StreamedContent getDownloadedFile() throws Exception{
        try{
            return new DefaultStreamedContent(new ByteArrayInputStream(this.imageInBytes), "image/jpg");     
        }catch(Exception e){
            throw e;
        }
    }



    /**
     * @return the imageInBytes
     */
    public byte[] getImageInBytes() {
        return imageInBytes;
    }

    /**
     * @param imageInBytes the imageInBytes to set
     */
    public void setImageInBytes(byte[] imageInBytes) {
        this.imageInBytes = imageInBytes;
    }

    /**
     * @return the hasImage
     */
    public boolean isHasImage() {
        return hasImage;
    }

    /**
     * @param hasImage the hasImage to set
     */
    public void setHasImage(boolean hasImage) {
        this.hasImage = hasImage;
    }
}
