/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.judicialito.model;

/**
 *
 * @author jacobotapia
 */
public class Denunciante {
    
    private String rfc,nombre,apellidoPaterno,apellidoMaterno,firma;
    private int id,direccionID;
    private Direccion direccion;
    
    public Denunciante(){
        this.direccion=new Direccion();
    }
    

    /**
     * @return the rfc
     */
    public String getRfc() {
        return rfc;
    }

    /**
     * @param rfc the rfc to set
     */
    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the apellidoPaterno
     */
    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    /**
     * @param apellidoPaterno the apellidoPaterno to set
     */
    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    /**
     * @return the apellidoMaterno
     */
    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    /**
     * @param apellidoMaterno the apellidoMaterno to set
     */
    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    /**
     * @return the firma
     */
    public String getFirma() {
        return firma;
    }

    /**
     * @param firma the firma to set
     */
    public void setFirma(String firma) {
        this.firma = firma;
    }

    /**
     * @return the direccion
     */
    public Direccion getDireccion() {
        return direccion;
    }

    /**
     * @param direccion the direccion to set
     */
    public void setDireccion(Direccion direccion) {
        this.direccion = direccion;
    }
    
    
    public void limpiar(){
        this.rfc=this.nombre=this.apellidoPaterno=
                this.apellidoMaterno=this.firma="";
        this.direccionID=1000;
        this.direccion.limpiar();
    }

    /**
     * @return the direccionID
     */
    public int getDireccionID() {
        return direccionID;
    }

    /**
     * @param direccionID the direccionID to set
     */
    public void setDireccionID(int direccionID) {
        this.direccionID = direccionID;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }
    
    @Override
    public String toString(){
        return this.nombre+" "+this.apellidoPaterno+" "+this.apellidoMaterno;
    }
    
    public String goodDirection(){
        String s=this.direccion.getCalle()+", #"+this.direccion.getNumeroExterior();
        if(this.direccion.getNumeroInterior()!=null || !this.direccion.getNumeroInterior().equals("")){
            s+=" - #"+this.direccion.getNumeroInterior();
        }
        s+=", Colonia: "+this.direccion.getColonia()+""
                + ", C.P:"+this.direccion.getCodigoPostal()+", "+this.direccion.getMunicipio()+" - "
                + ""+this.direccion.getEstado();        
        return s;
    }
    
    
}
