/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.judicialito.model;

import java.util.Date;

/**
 *
 * @author jacobotapia
 */
public class Infractor {
    private int id,edad;
    private String rfc,nombre,apellidoPaterno,apellidoMaterno,genero,etnia,
            nivelEducacion,religion;
    private Date fechaNacimiento;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the edad
     */
    public int getEdad() {       
	long ageInMillis = new Date().getYear() - this.fechaNacimiento.getYear();
	return (int) ageInMillis;
    }

    /**
     * @param edad the edad to set
     */
    public void setEdad(int edad) {
        this.edad = edad;
    }

    /**
     * @return the rfc
     */
    public String getRfc() {
        return rfc;
    }

    /**
     * @param rfc the rfc to set
     */
    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the apellidoPaterno
     */
    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    /**
     * @param apellidoPaterno the apellidoPaterno to set
     */
    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    /**
     * @return the apellidoMaterno
     */
    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    /**
     * @param apellidoMaterno the apellidoMaterno to set
     */
    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    /**
     * @return the genero
     */
    public String getGenero() {
        return genero;
    }

    /**
     * @param genero the genero to set
     */
    public void setGenero(String genero) {
        this.genero = genero;
    }

    /**
     * @return the etnia
     */
    public String getEtnia() {
        return etnia;
    }

    /**
     * @param etnia the etnia to set
     */
    public void setEtnia(String etnia) {
        this.etnia = etnia;
    }

    /**
     * @return the nivelEducacion
     */
    public String getNivelEducacion() {
        return nivelEducacion;
    }

    /**
     * @param nivelEducacion the nivelEducacion to set
     */
    public void setNivelEducacion(String nivelEducacion) {
        this.nivelEducacion = nivelEducacion;
    }

    /**
     * @return the religion
     */
    public String getReligion() {
        return religion;
    }

    /**
     * @param religion the religion to set
     */
    public void setReligion(String religion) {
        this.religion = religion;
    }

    /**
     * @return the fechaNacimiento
     */
    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    /**
     * @param fechaNacimiento the fechaNacimiento to set
     */
    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }
    
    public void limpiar(){
        this.nombre=this.apellidoPaterno=this.apellidoMaterno=
                this.rfc="";
        this.etnia=this.nivelEducacion="Ninguno";
        this.religion="Ninguna";
        this.fechaNacimiento=null;
    }
    
    @Override
    public String toString(){
        String s=this.rfc+": "+this.nombre+" "+this.apellidoPaterno+" "+this.apellidoMaterno;
        if(s.equals("null: null null null")){
            return "Persona no encontrada";
        }else{
            return s;
        }
    }
    
    public String justName(){
        return this.nombre+" "+this.apellidoPaterno+" "+this.apellidoMaterno;
    }
}
