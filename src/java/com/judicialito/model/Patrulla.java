/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.judicialito.model;

/**
 *
 * @author jacobotapia
 */

public class Patrulla {
    
    private int id,sectorID;
    private String placas,marca,modelo;
    private Sector sector;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the sectorID
     */
    public int getSectorID() {
        return sectorID;
    }

    /**
     * @param sectorID the sectorID to set
     */
    public void setSectorID(int sectorID) {
        this.sectorID = sectorID;
    }

    /**
     * @return the placas
     */
    public String getPlacas() {
        return placas;
    }

    /**
     * @param placas the placas to set
     */
    public void setPlacas(String placas) {
        this.placas = placas;
    }

    /**
     * @return the marca
     */
    public String getMarca() {
        return marca;
    }

    /**
     * @param marca the marca to set
     */
    public void setMarca(String marca) {
        this.marca = marca;
    }

    /**
     * @return the modelo
     */
    public String getModelo() {
        return modelo;
    }

    /**
     * @param modelo the modelo to set
     */
    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    /**
     * @return the sector
     */
    public Sector getSector() {
        return sector;
    }

    /**
     * @param sector the sector to set
     */
    public void setSector(Sector sector) {
        this.sector = sector;
    }
    
    public void limpiar(){
        this.placas=this.marca=this.modelo="";
        this.sectorID=1;
    }
    
    @Override
    public String toString(){
        String s=this.placas+" "+this.marca+" "+this.modelo;
        if(s.equals("null null null")){
            return "Patrulla no encontrada";
        }else{
            return s;
        }
    }
}
