/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.judicialito.model;

/**
 *
 * @author jacobotapia
 */
public class Testigo extends Denunciante{
    
    private String testimonio;
    private int reporteID;

    /**
     * @return the testimonio
     */
    public String getTestimonio() {
        return testimonio;
    }

    /**
     * @param testimonio the testimonio to set
     */
    public void setTestimonio(String testimonio) {
        this.testimonio = testimonio;
    }

    /**
     * @return the reporteID
     */
    public int getReporteID() {
        return reporteID;
    }

    /**
     * @param reporteID the reporteID to set
     */
    public void setReporteID(int reporteID) {
        this.reporteID = reporteID;
    }
    
    
}
