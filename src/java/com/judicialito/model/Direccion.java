/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.judicialito.model;

/**
 *
 * @author jacobotapia
 */
public class Direccion {
    private int codigoPostal; 
    private String colonia,estado,municipio,calle,
            numeroInterior,numeroExterior;
    
    public Direccion(){
    }
    

    /**
     * @return the codigoPostal
     */
    public int getCodigoPostal() {
        return codigoPostal;
    }

    /**
     * @param codigoPostal the codigoPostal to set
     */
    public void setCodigoPostal(int codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    /**
     * @return the colonia
     */
    public String getColonia() {
        return colonia;
    }

    /**
     * @param colonia the colonia to set
     */
    public void setColonia(String colonia) {
        this.colonia = colonia;
    }

    /**
     * @return the estado
     */
    public String getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     * @return the municipio
     */
    public String getMunicipio() {
        return municipio;
    }

    /**
     * @param municipio the municipio to set
     */
    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    /**
     * @return the calle
     */
    public String getCalle() {
        return calle;
    }

    /**
     * @param calle the calle to set
     */
    public void setCalle(String calle) {
        this.calle = calle;
    }

    /**
     * @return the numeroInterior
     */
    public String getNumeroInterior() {
        return numeroInterior;
    }

    /**
     * @param numeroInterior the numeroInterior to set
     */
    public void setNumeroInterior(String numeroInterior) {
        this.numeroInterior = numeroInterior;
    }

    /**
     * @return the numeroExterior
     */
    public String getNumeroExterior() {
        return numeroExterior;
    }

    /**
     * @param numeroExterior the numeroExterior to set
     */
    public void setNumeroExterior(String numeroExterior) {
        this.numeroExterior = numeroExterior;
    }
    
    public void limpiar(){
        this.colonia=this.estado=this.municipio=this.calle=
                this.numeroInterior=this.numeroExterior="";
        this.codigoPostal=0;
    }
    
    @Override
    public String toString(){
        String s="Estado: "+this.estado+", Municipio: "+this.municipio+""
                + ", Colonia: "+this.colonia+", Código Postal: "+this.codigoPostal;
        if(s.equals("Estado: No encontrado, Municipio: No encontrado, "
                + "Colonia: No encontrado, Código Postal: 0") || s.equals(
                "Estado: null, Municipio: null, Colonia: null, Código Postal: 0") ||
                s.equals("Estado: , Municipio: , Colonia: , Código Postal: 0")){
            return "Dirección no encontrada";
        }else{
            return s;
        }
    }
    
}
