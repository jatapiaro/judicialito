/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.judicialito.model;

/**
 *
 * @author jacobotapia
 */
public class Foto {
    private int id,reporteID;
    private String nombre,descripcion;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    public void limpiar(){
        this.nombre=this.descripcion="";
    }

    /**
     * @return the reporteID
     */
    public int getReporteID() {
        return reporteID;
    }

    /**
     * @param reporteID the reporteID to set
     */
    public void setReporteID(int reporteID) {
        this.reporteID = reporteID;
    }
}
