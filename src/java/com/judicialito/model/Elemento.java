/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.judicialito.model;

/**
 *
 * @author jacobotapia
 */

public class Elemento {
    private int id,nivelAcceso,sectorID;
    private String nombre,apellidoPaterno,apellidoMaterno,placa,password;
    private Sector sector;
    
    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the nivelAcceso
     */
    public int getNivelAcceso() {
        return nivelAcceso;
    }

    /**
     * @param nivelAcceso the nivelAcceso to set
     */
    public void setNivelAcceso(int nivelAcceso) {
        this.nivelAcceso = nivelAcceso;
    }

    /**
     * @return the sectorID
     */
    public int getSectorID() {
        return sectorID;
    }

    /**
     * @param sectorID the sectorID to set
     */
    public void setSectorID(int sectorID) {
        this.sectorID = sectorID;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the apellidoPaterno
     */
    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    /**
     * @param apellidoPaterno the apellidoPaterno to set
     */
    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    /**
     * @return the apellidoMaterno
     */
    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    /**
     * @param apellidoMaterno the apellidoMaterno to set
     */
    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    /**
     * @return the placa
     */
    public String getPlaca() {
        return placa;
    }

    /**
     * @param placa the placa to set
     */
    public void setPlaca(String placa) {
        this.placa = placa;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the sector
     */
    public Sector getSector() {
        return sector;
    }

    /**
     * @param sector the sector to set
     */
    public void setSector(Sector sector) {
        this.sector = sector;
    }
    
    public void limpiar(){
        this.nombre=this.apellidoPaterno=
                this.apellidoMaterno=this.placa=this.password="";
        this.sectorID=1;
        this.nivelAcceso=0;
    }
    
    @Override
    public String toString(){
        String s=this.nombre+" "+this.apellidoPaterno+" "+this.apellidoMaterno+" del Sector "+this.sector;
        if(s.equals("null null null del Sector null")){
            return "Elemento policial no encontrado";
        }else{
            return s;
        }
    }
    
    public String soloNombre(){
        return this.nombre+" "+this.apellidoPaterno+" "+this.apellidoMaterno;
    }
}
